Uruchamianie: ./compiler [-stdin|plik.imp] [-stdout|plik.out]

Program może przyjać wejście ze standardowego inputu jak również z podanego w pierwszym argumencie pliku wejściowego.
Output może zostać wysłany do pliku lub na standardowe wyjście.

Pętle for wykonują się [min, max], obie części są włączne.
Nie ma wskazywania na miejsce błędu.
Nie ma sprawdzania czy dostęp do tablicy przekroczył zakres (dla kompilatora).

Kompilacja odbywa się za pomocą komendy make.
Nie są wymagane inne biblioteki i programy za wyjątkiem libcln, bison i flex.

Możliwe jest użycie komendy make debug, aczkolwiek jedyne co dodaje ona w obecnym statusie
to tryb debugowania dla bisona i flexa.