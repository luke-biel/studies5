#include "Var.hpp"

cln::cl_I Var::s_count = 0;

cln::cl_I Var::next() {
  s_count += 1;
  return s_count;
}
