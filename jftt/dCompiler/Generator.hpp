#ifndef GENERATOR_HPP
#define GENERATOR_HPP

#include <list>
#include <vector>
#include <ostream>

#include "Var.hpp"
#include "Command.hpp"

class Generator {
private:
  std::list<Var*> m_vars;
  Commands *m_commands;

  Generator();
  Generator(const Generator&) = delete;
  void operator=(const Generator&) = delete;

public:
  static Generator &getInstance();

  // Registering variables
  void addVar(Var *var);
  void remVar();
  Var *findVar(Var *var);
  Var *findVar(std::string name);

  // Commands
  void addCommands(Commands *cmd);
  void dumpCommands();

  // Generating
  void generate(std::ostream *out);

  static std::string getUniqueIndex();
  
  static void loadMemToReg(std::list<Asm*> &machineCmds, Ref *ref, int reg);
  static void saveRegToMem(std::list<Asm*> &machimeCmds, Ref *ref, int reg);
  static void evalExpToReg(std::list<Asm*> &machineCmds, Exp *exp, int reg);
  static void loadPtrToReg(std::list<Asm*> &machineCmds, Ref *exp, int reg);
  static void evalCondJumpTo(std::list<Asm *> &machineCmds, Cond *cnd, std::string &then, std::string &othw);

  static void setToNumber(std::list<Asm*> &machineCmds, cln::cl_I n, int reg);
  static void addToNumber(std::list<Asm*> &machineCmds, cln::cl_I n, int reg);
  
  static void add(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst);
  static void sub(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst);
  static void mul(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst);
  static void div(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst);
  static void mod(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst);

  static void eq(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst, std::string &then, std::string &othw);
  static void neq(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst, std::string &then, std::string &othw);
  static void lt(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst, std::string &then, std::string &othw);
  static void gt(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst, std::string &then, std::string &othw);
  static void leq(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst, std::string &then, std::string &othw);
  static void geq(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst, std::string &then, std::string &othw);

};

#define GEN Generator::getInstance()

#endif
