#ifndef EXP_HPP
#define EXP_HPP

#include "Util.hpp"
#include "Ref.hpp"

enum Operator {
  NONE, ADD, SUB, MUL, DIV, MOD
};

class Exp {
  KLASS(Exp);
private:
  Ref *m_fst, *m_snd;
  Operator m_op;

public:
  Exp(Ref *fst, Operator op = NONE, Ref *snd = nullptr) :
    m_fst(fst),
    m_snd(snd),
    m_op(op) { }

  virtual ~Exp() { delete m_fst; delete m_snd; }

  Operator getOp() { return m_op; }
  Ref *getFst() { return m_fst; }
  Ref *getSnd() { return m_snd; }
};

#endif
