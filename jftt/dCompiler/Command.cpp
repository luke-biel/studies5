#include "Command.hpp"

#include <sstream>

#include "Debug.hpp"
#include "Generator.hpp"

void Commands::add(Command *cmd) {
  DEBUG(this << " is adding new cmd (" << cmd->name() << ", " << cmd << ")");
  m_commands.push_back(cmd);
}

std::list<Command*>::iterator Commands::begin() {
  return m_commands.begin();
}

std::list<Command*>::iterator Commands::end() {
  return m_commands.end();
}

void Assign::translate(std::list<Asm*> &machineCmds) {
  if(m_where->name() == Const::klass()) {
	throw std::runtime_error("Cannot assign Const!");
  } else {
	  Var *v;
	  if(m_where->name() == Token::klass()) {
		  v = GEN.findVar(dynamic_cast<Token*>(m_where)->get());
	  } else {
		  v = GEN.findVar(dynamic_cast<TablePtr*>(m_where)->getName());
	  }
	  if(v->name() == Iterator::klass()) {
		  throw std::runtime_error("Iterators are readonly");
	  }
  }
  Generator::evalExpToReg(machineCmds, m_what, 1);
  Generator::saveRegToMem(machineCmds, m_where, 1);
}

void If::translate(std::list<Asm*> &machineCmds) {
  std::string then = GEN.getUniqueIndex(), othw = GEN.getUniqueIndex();
  std::string end = GEN.getUniqueIndex();
  Generator::evalCondJumpTo(machineCmds, m_cond, then, othw);
  machineCmds.push_back(new Asm("ZERO 0", then));
  for(Command *c : *m_then) {
	  c->translate(machineCmds);
  }
  machineCmds.push_back(new Asm("JUMP {" + end + "}"));
  machineCmds.push_back(new Asm("ZERO 0", othw));
  for(Command *c : *m_othw) {
	  c->translate(machineCmds);
  }
  machineCmds.push_back(new Asm("ZERO 0", end));
}

void While::translate(std::list<Asm*> &machineCmds) {
	std::string begin = GEN.getUniqueIndex();
	std::string end = GEN.getUniqueIndex();
	std::string then = GEN.getUniqueIndex();
	machineCmds.push_back(new Asm("ZERO 0", begin));
	Generator::evalCondJumpTo(machineCmds, m_cond, then, end);
	machineCmds.push_back(new Asm("ZERO 0", then));
	for(Command *c : *m_then) {
		c->translate(machineCmds);
	}
	machineCmds.push_back(new Asm("JUMP {" + begin + "}"));
	machineCmds.push_back(new Asm("ZERO 0", end));
}

void ForUp::translate(std::list<Asm*> &machineCmds) {
	Ref *bgn = m_it->getMin();
	Ref *end = m_it->getMax();
	GEN.addVar(m_it);
	Integer *counter = new Integer(GEN.getUniqueIndex());
	if(bgn->name() == Const::klass()) {
		GEN.loadMemToReg(machineCmds, bgn, 1);
		GEN.setToNumber(machineCmds, counter->getPtr(), 0);
		machineCmds.push_back(new Asm("STORE 1"));

		GEN.loadMemToReg(machineCmds, end, 1);
		machineCmds.push_back(new Asm("INC 1"));
		GEN.setToNumber(machineCmds, counter->getPtr(), 0);

		machineCmds.push_back(new Asm("SUB 1"));
		machineCmds.push_back(new Asm("STORE 1"));
	} else {
		GEN.loadMemToReg(machineCmds, end, 1);
		machineCmds.push_back(new Asm("INC 1"));
		GEN.loadPtrToReg(machineCmds, bgn, 0);
			
		machineCmds.push_back(new Asm("SUB 1"));
		GEN.setToNumber(machineCmds, counter->getPtr(), 0);
			
		machineCmds.push_back(new Asm("STORE 1"));
	}
	GEN.loadMemToReg(machineCmds, bgn, 1);
	GEN.setToNumber(machineCmds, m_it->getPtr(), 0);
	machineCmds.push_back(new Asm("STORE 1"));

	std::string exit = GEN.getUniqueIndex();
	std::string back = GEN.getUniqueIndex();

	machineCmds.push_back(new Asm("ZERO 0", back));
	GEN.setToNumber(machineCmds, counter->getPtr(), 0);
	machineCmds.push_back(new Asm("LOAD 1"));
	machineCmds.push_back(new Asm("JZERO 1 {" + exit + "}"));
	for(Command *c : *m_then) {
		c->translate(machineCmds);
	}
	GEN.setToNumber(machineCmds, counter->getPtr(), 0);
	machineCmds.push_back(new Asm("LOAD 1"));
	machineCmds.push_back(new Asm("DEC 1"));
	machineCmds.push_back(new Asm("STORE 1"));
	GEN.setToNumber(machineCmds, m_it->getPtr(), 0);
	machineCmds.push_back(new Asm("LOAD 1"));
	machineCmds.push_back(new Asm("INC 1"));
	machineCmds.push_back(new Asm("STORE 1"));
	machineCmds.push_back(new Asm("JUMP {" + back + "}"));
	machineCmds.push_back(new Asm("ZERO 0", exit));
	
	
	GEN.remVar();
}

void ForDown::translate(std::list<Asm*> &machineCmds) {
	Ref *bgn = m_it->getMin();
	Ref *end = m_it->getMax();
	GEN.addVar(m_it);
	Integer *counter = new Integer(GEN.getUniqueIndex());
	if(bgn->name() == Const::klass()) {
		GEN.loadMemToReg(machineCmds, end, 1);
		GEN.setToNumber(machineCmds, counter->getPtr(), 0);
		machineCmds.push_back(new Asm("STORE 1"));

		GEN.loadMemToReg(machineCmds, bgn, 1);
		machineCmds.push_back(new Asm("INC 1"));
		GEN.setToNumber(machineCmds, counter->getPtr(), 0);

		machineCmds.push_back(new Asm("SUB 1"));
		machineCmds.push_back(new Asm("STORE 1"));
	} else {
		GEN.loadMemToReg(machineCmds, bgn, 1);
		machineCmds.push_back(new Asm("INC 1"));
		GEN.loadPtrToReg(machineCmds, end, 0);
			
		machineCmds.push_back(new Asm("SUB 1"));
		GEN.setToNumber(machineCmds, counter->getPtr(), 0);
			
		machineCmds.push_back(new Asm("STORE 1"));
	}
	GEN.loadMemToReg(machineCmds, bgn, 1);
	GEN.setToNumber(machineCmds, m_it->getPtr(), 0);
	machineCmds.push_back(new Asm("STORE 1"));

	std::string exit = GEN.getUniqueIndex();
	std::string back = GEN.getUniqueIndex();

	machineCmds.push_back(new Asm("ZERO 0", back));
	GEN.setToNumber(machineCmds, counter->getPtr(), 0);
	machineCmds.push_back(new Asm("LOAD 1"));
	machineCmds.push_back(new Asm("JZERO 1 {" + exit + "}"));
	for(Command *c : *m_then) {
		c->translate(machineCmds);
	}
	GEN.setToNumber(machineCmds, counter->getPtr(), 0);
	machineCmds.push_back(new Asm("LOAD 1"));
	machineCmds.push_back(new Asm("DEC 1"));
	machineCmds.push_back(new Asm("STORE 1"));
	GEN.setToNumber(machineCmds, m_it->getPtr(), 0);
	machineCmds.push_back(new Asm("LOAD 1"));
	machineCmds.push_back(new Asm("DEC 1"));
	machineCmds.push_back(new Asm("STORE 1"));
	machineCmds.push_back(new Asm("JUMP {" + back + "}"));
	machineCmds.push_back(new Asm("ZERO 0", exit));
	delete counter;

	GEN.remVar();  
}

void Read::translate(std::list<Asm*> &machineCmds) {
  machineCmds.push_back(new Asm("GET 1"));
  Generator::saveRegToMem(machineCmds, m_id, 1);
}

void Write::translate(std::list<Asm*> &machineCmds) {
  Generator::loadMemToReg(machineCmds, m_val, 1);
  machineCmds.push_back(new Asm("PUT 1"));
}

void Skip::translate(std::list<Asm*> &machineCmds) {
  machineCmds.push_back(new Asm("ZERO 0"));
}
