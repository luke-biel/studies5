#include "Generator.hpp"

#include <map>
#include <string>
#include <algorithm>
#include <exception>
#include <sstream>
#include <regex>

#include "Debug.hpp"

extern void yyerror(char const *);

Generator::Generator() {

}

Generator &Generator::getInstance() {
  static Generator instance;
  return instance;
}

void Generator::addVar(Var *var) {
  if(findVar(var)) {
    throw std::runtime_error("Variable with same name (" +
			     var->getName() +
			     ") already exists in current context!");
  }
  m_vars.push_back(var);
}

void Generator::remVar() {
  Var *v = m_vars.back();
  m_vars.pop_back();
  delete v;
}

Var *Generator::findVar(Var *var) {
  std::list<Var*>::iterator it =
    std::find_if(m_vars.begin(), m_vars.end(), [&](Var *other) {
	return !other->getName().compare(var->getName());
      });
  return it == m_vars.end() ? nullptr : *it;
}

Var *Generator::findVar(std::string name) {
  std::list<Var*>::iterator it =
    std::find_if(m_vars.begin(), m_vars.end(), [&](Var *other) {
	return !other->getName().compare(name);
      });
  return it == m_vars.end() ? nullptr : *it;
}

void Generator::addCommands(Commands *cmd) {
  m_commands = cmd;
}

void dumpCommands(Commands *cmds, int tab = 0) {
  for(auto cmd : *cmds) {
    std::string name = cmd->name();
    std::cerr << std::string(tab*2, ' ') << name << std::endl;
    if(name == If::klass()) {
      If *cast = dynamic_cast<If*>(cmd);
      std::cerr << std::string(tab * 2 + 1, ' ') << "then: " << std::endl;
      dumpCommands(cast->getThen(), tab + 1);
      std::cerr << std::string(tab * 2 + 1, ' ') << "else: " << std::endl;
      dumpCommands(cast->getElse(), tab + 1);
    } else if(name == While::klass()) {
      While *cast = dynamic_cast<While*>(cmd);
      std::cerr << std::string(tab * 2 + 1, ' ') << "do: " << std::endl;
      dumpCommands(cast->getThen(), tab + 1);
    } else if(name == ForDown::klass()) {
      ForDown *cast = dynamic_cast<ForDown*>(cmd);
      std::cerr << std::string(tab * 2 + 1, ' ') << "do: " << std::endl;
      dumpCommands(cast->getThen(), tab + 1);
    } else if(name == ForUp::klass()) {
      ForUp *cast = dynamic_cast<ForUp*>(cmd);
      std::cerr << std::string(tab * 2 + 1, ' ') << "do: " << std::endl;
      dumpCommands(cast->getThen(), tab + 1);
    }
  }
}

void Generator::dumpCommands() {
  ::dumpCommands(m_commands);
}

void Generator::generate(std::ostream *out) {
  std::list<Asm*> machineCmds;
  for(Command *c : *m_commands) {
	c->translate(machineCmds);
  }
  std::map<std::string, int> indices;
  int idx = 0;
  // Indexing labels
  for(std::list<Asm*>::iterator i = machineCmds.begin(); i != machineCmds.end(); i++, idx++) {
	  if((*i)->getLabel() != "") {
		  indices[(*i)->getLabel()] = idx;
	  }
  }
  std::regex cb("\\{[0-9]*\\}");
  for(std::list<Asm*>::iterator i = machineCmds.begin(); i != machineCmds.end(); i++) {
	  std::smatch sm;
	  std::string cmd = (*i)->getCmd();
	  int line = 0;
	  if(std::regex_search(cmd, sm, cb)) {
		  std::string jumpTo = sm.str();
		  jumpTo.erase(0, 1);
		  jumpTo.erase(jumpTo.size() - 1);
		  line = indices[jumpTo];
	  }	  
	  (*out) << std::regex_replace(cmd, cb, std::to_string(line)) << std::endl;
  }
  // for(std::list<Asm*>::iterator i = machineCmds.begin(); i != machineCmds.end(); i++) {
  // 	Asm *a = *i;
  // 	(*out) << a->getCmd() << std::endl;
  // }
  (*out) << "HALT" << std::endl;
}

std::string Generator::getUniqueIndex() {
  static int id = 0;
  return std::to_string(id++);
}

void Generator::loadMemToReg(std::list<Asm*> &machineCmds, Ref *ref, int reg) {
  if(ref->name() == Const::klass()) {
	/**
	 * 1 + 1 + 1 + 1... lands in registry 'reg'
	 */
    Const *c = dynamic_cast<Const*>(ref);
	setToNumber(machineCmds, c->get(), reg);
  } else if(ref->name() == Token::klass()) {
	/**
	 * We load what lies at variable location to 'reg'
	 */
    Var *v = GEN.findVar(dynamic_cast<Token*>(ref)->get());
	setToNumber(machineCmds, v->getPtr(), 0);
    machineCmds.push_back(new Asm("LOAD " + std::to_string(reg)));
  } else if(ref->name() == TablePtr::klass()) {
	/**
	 * First we load accessor at registry 0 and then increase it by table pos.
	 * Then we load what liest there to 'reg'
	 */
	TablePtr *tptr = dynamic_cast<TablePtr*>(ref);
    Table *v = dynamic_cast<Table*>(GEN.findVar(tptr->getName()));
	if(tptr->getPtr()->name() == TablePtr::klass()) {
	  throw std::runtime_error("Cannot access table with another table pointer! " \
							   "Use intermediate variable instead!");
	}
	
	loadMemToReg(machineCmds, tptr->getPtr(), 0);
	addToNumber(machineCmds, v->getPtr(), 0);

	machineCmds.push_back(new Asm("LOAD " + std::to_string(reg)));
  } else {
    throw std::runtime_error("Unknown reference type!");
  }
}

/**
 * Limitations: Cannot save anything from 0th registry.
 */
void Generator::saveRegToMem(std::list<Asm *> &machimeCmds, Ref *ref, int reg) {
  if(ref->name() == Const::klass()) {
	/**
	 * We cannot assign to constant
	 */
	throw std::runtime_error("Invalid assignement!");
  } else if(ref->name() == Token::klass()) {
	Token *tok = dynamic_cast<Token*>(ref);
	Var *v = GEN.findVar(tok->get());
	if(!v || v->name() != Integer::klass()) {
	  throw std::runtime_error("Variable is either not devlared or is of invalid type!");
	}
	Integer *integr = dynamic_cast<Integer*>(v);
	Const *cnst = new Const(integr->getPtr());
	loadMemToReg(machimeCmds, cnst, 0);
	machimeCmds.push_back(new Asm("STORE " + std::to_string(reg)));
	delete cnst;
  } else {
	TablePtr *tab = dynamic_cast<TablePtr*>(ref);
	Var *v = GEN.findVar(tab->getName());
	if(!v || v->name() != Table::klass()) {
	  throw std::runtime_error("Variable is either not devlared or is of invalid type!");
	}
	Table *table = dynamic_cast<Table*>(v);
	loadMemToReg(machimeCmds, tab->getPtr(), 0);
	for(cln::cl_I i = 0; i < table->getPtr(); i++) {
	  machimeCmds.push_back(new Asm("INC 0"));
	}
	machimeCmds.push_back(new Asm("STORE " + std::to_string(reg)));
  }
}

void Generator::evalExpToReg(std::list<Asm *> &machineCmds, Exp *exp, int reg) {
  switch(exp->getOp()) {
  case ADD: {
	add(machineCmds, exp->getFst(), exp->getSnd());
  } break;
  case SUB: {
	sub(machineCmds, exp->getFst(), exp->getSnd());
  } break;
  case MUL: {
	mul(machineCmds, exp->getFst(), exp->getSnd());
  } break;
  case DIV: {
	div(machineCmds, exp->getFst(), exp->getSnd());
  } break;
  case MOD: {
	mod(machineCmds, exp->getFst(), exp->getSnd());
  } break;
  case NONE:
  default: {
	loadMemToReg(machineCmds, exp->getFst(), reg);
  } break;
  }
}

void Generator::evalCondJumpTo(std::list<Asm *> &machineCmds, Cond *cnd, std::string &then, std::string &othw) {
  if(cnd->name() == Eq::klass()) {
	eq(machineCmds, cnd->fst(), cnd->snd(), then, othw);
  } else if(cnd->name() == Neq::klass()) {
	neq(machineCmds, cnd->fst(), cnd->snd(), then, othw);
  } else if(cnd->name() == Lt::klass()) {
	lt(machineCmds, cnd->fst(), cnd->snd(), then, othw);
  } else if(cnd->name() == Gt::klass()) {
	gt(machineCmds, cnd->fst(), cnd->snd(), then, othw);
  } else if(cnd->name() == Leq::klass()) {
	leq(machineCmds, cnd->fst(), cnd->snd(), then, othw);
  } else if(cnd->name() == Geq::klass()) {
	geq(machineCmds, cnd->fst(), cnd->snd(), then, othw);
  } else {
	throw std::runtime_error("Invalid condition!");
  }
}

void Generator::loadPtrToReg(std::list<Asm*> &machineCmds, Ref *ref, int reg) {
  if(reg < 0 || reg > 4) {
	throw std::runtime_error("Invalid registry!");
  }
  std::string regName = std::to_string(reg);
  if(ref->name() == Const::klass()) {
	  machineCmds.push_back(new Asm("ZERO 0"));
  } else if(ref->name() == Token::klass()) {
	machineCmds.push_back(new Asm("ZERO " + regName));
	std::string varName = dynamic_cast<Token*>(ref)->get();
	Var *v = GEN.findVar(varName);
	if(!v || (v->name() != Integer::klass() && v->name() != Iterator::klass())) {
	  throw std::runtime_error("Variable (" + varName + ") is either undeclared or is not an Integer!");
	}
	for(cln::cl_I i = 0; i < v->getPtr(); i++) {
	  machineCmds.push_back(new Asm("INC " + regName));
	}
  } else {
	TablePtr *tptr = dynamic_cast<TablePtr*>(ref);
	Ref *ptr = tptr->getPtr();
	if(ptr->name() == TablePtr::klass()) {
	  throw std::runtime_error("Invalid access variable for table!");
	}
	loadPtrToReg(machineCmds, ptr, reg);
	Var *v = GEN.findVar(tptr->getName());
	if(!v || v->name() != Table::klass()) {
	  throw std::runtime_error("Undeclared or invalid type variable!");
	}
	for(cln::cl_I i = 0; i < v->getPtr(); i++) {
	  machineCmds.push_back(new Asm("INC " + regName));
	}
  }
}

void Generator::setToNumber(std::list<Asm *> &machineCmds, cln::cl_I n, int reg) {
  if(reg < 0 || reg > 4) {
	throw std::runtime_error("Invalid registry!");
  }
  std::ostringstream ss;
  cln::fprintbinary(ss, n);
  std::string intgr = ss.str();
  std::string regName = std::to_string(reg);
  machineCmds.push_back(new Asm("ZERO " + regName));
  bool began = false;
  for(char c : intgr) {
	  if(began) {
		  machineCmds.push_back(new Asm("SHL " + regName));
	  }
	  if(c == '1') {
		  machineCmds.push_back(new Asm("INC " + regName));
		  began = true;
	  }
  }
}

void Generator::addToNumber(std::list<Asm *> &machineCmds, cln::cl_I n, int reg) {
  if(reg < 0 || reg > 4) {
	throw std::runtime_error("Invalid registry!");
  }
  std::ostringstream ss;
  cln::fprintbinary(ss, n);
  std::string intgr = ss.str();
  std::string regName = std::to_string(reg);
  bool began = false;
  for(char c : intgr) {
	  if(began) {
		  machineCmds.push_back(new Asm("SHL " + regName));
	  }
	  if(c == '1') {
		  machineCmds.push_back(new Asm("INC " + regName));
		  began = true;
	  }
  }
}

void Generator::add(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst) {
  if(fst->name() == Const::klass()) {
	if(lst->name() != Const::klass()) {
	  add(machineCmds, lst, fst);
	} else {
	  Const *c1 = dynamic_cast<Const*>(fst);
	  Const *c2 = dynamic_cast<Const*>(lst);
	  setToNumber(machineCmds, c1->get() + c2->get(), 1);
	}
  } else if(lst->name() == Const::klass()) { 
	loadMemToReg(machineCmds, fst, 1);
	Const *c = dynamic_cast<Const*>(lst);
	setToNumber(machineCmds, c->get(), 1);
  } else {
	loadMemToReg(machineCmds, fst, 1);
	loadPtrToReg(machineCmds, lst, 0);
	machineCmds.push_back(new Asm("ADD 1"));
  }
}

void Generator::sub(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst) {
  if(lst->name() == Const::klass()) {
	if(fst->name() == Const::klass()) {
	  Const *c = dynamic_cast<Const*>(fst);
	  cln::cl_I val = c->get();
	  c = dynamic_cast<Const*>(lst);
	  val -= c->get();
	  setToNumber(machineCmds, val, 1);
	} else {
	  loadMemToReg(machineCmds, fst, 1);
	  for(cln::cl_I i = 0; i < dynamic_cast<Const*>(lst)->get(); i++) {
		machineCmds.push_back(new Asm("DEC 1"));
	  }
	}
  } else {
	loadMemToReg(machineCmds, fst, 1);
	loadPtrToReg(machineCmds, lst, 0);
	machineCmds.push_back(new Asm("SUB 1"));
  }
}

void Generator::mul(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst) {
  if(fst->name() == Const::klass()) {
	if(lst->name() == Const::klass()) { // We do it Just in Time, because resons
	  cln::cl_I a = dynamic_cast<Const*>(fst)->get();
	  cln::cl_I b = dynamic_cast<Const*>(lst)->get();
	  if(b > a) {
		cln::cl_I tmp = a;
		a = b;
		b = tmp;
	  }
	  if(a > b) {
		setToNumber(machineCmds, a, 1);
		machineCmds.push_back(new Asm("ZERO 0"));
		machineCmds.push_back(new Asm("STORE 1"));
		machineCmds.push_back(new Asm("ZERO 1"));
		for(cln::cl_I i = 0; i < b; i++) {
		  machineCmds.push_back(new Asm("ADD 1"));
		}
	  }
	} else {
	  mul(machineCmds, lst, fst);
	}
  } else {
	if(lst->name() == Const::klass()) {
	  machineCmds.push_back(new Asm("ZERO 1"));
	  loadPtrToReg(machineCmds, fst, 0);
	  for(cln::cl_I i = 0; i < dynamic_cast<Const*>(lst)->get(); i++) {
		  machineCmds.push_back(new Asm("ADD 1"));
	  }
	} else {
	  std::string jumpTo = getUniqueIndex();
	  std::string jumpBack = getUniqueIndex();
	  machineCmds.push_back(new Asm("ZERO 1"));
	  loadPtrToReg(machineCmds, fst, 0);
	  loadMemToReg(machineCmds, lst, 4);
	  machineCmds.push_back(new Asm("JZERO 4 {" + jumpTo + "}", jumpBack));
	  machineCmds.push_back(new Asm("DEC 4"));
	  machineCmds.push_back(new Asm("ADD 1"));
	  machineCmds.push_back(new Asm("JUMP {" + jumpBack + "}"));
	  machineCmds.push_back(new Asm("ZERO 0", jumpTo));
	}
  }
}

void Generator::div(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst) {
  if(fst->name() == Const::klass() && lst->name() == Const::klass()) {
	// Here on the other side AoT
	cln::cl_I val = cln::exquo(dynamic_cast<Const*>(fst)->get(),
							   dynamic_cast<Const*>(lst)->get());
	setToNumber(machineCmds, val, 1);
  } else {
	  cln::cl_I ptr;
	  if(lst->name() == Const::klass()) {
		  loadMemToReg(machineCmds, lst, 1);
		  machineCmds.push_back(new Asm("ZERO 0"));
		  machineCmds.push_back(new Asm("STORE 1"));
		  ptr = 0;
	  } else {
		  std::string varName;
		  if(lst->name() == Token::klass()) {
			  varName = dynamic_cast<Token*>(lst)->get();
		  } else {
			  varName = dynamic_cast<TablePtr*>(lst)->getName();
		  }
		  ptr = GEN.findVar(varName)->getPtr();
	  }
	  std::string back = getUniqueIndex();
	  std::string end = getUniqueIndex();
	  std::string inc = getUniqueIndex();
	  std::string exit = getUniqueIndex();
	  loadMemToReg(machineCmds, lst, 1);
	  machineCmds.push_back(new Asm("JZERO 1 {" + exit + "}"));
	  
	  loadMemToReg(machineCmds, fst, 2);
	  loadMemToReg(machineCmds, fst, 3);
	  setToNumber(machineCmds, 0, 1);
	  setToNumber(machineCmds, ptr, 0);
	  machineCmds.push_back(new Asm("ZERO 4", back));
	  machineCmds.push_back(new Asm("SUB 2"));
	  machineCmds.push_back(new Asm("JZERO 2 {" + end + "}"));
	  machineCmds.push_back(new Asm("SUB 3"));
	  machineCmds.push_back(new Asm("INC 1"));
	  machineCmds.push_back(new Asm("JUMP {" + back + "}"));
	  machineCmds.push_back(new Asm("ZERO 0", end));
	  loadMemToReg(machineCmds, lst, 2);
	  machineCmds.push_back(new Asm("ZERO 0"));
	  machineCmds.push_back(new Asm("STORE 3"));
	  machineCmds.push_back(new Asm("SUB 2"));
	  machineCmds.push_back(new Asm("JZERO 2 {" + inc + "}"));
	  machineCmds.push_back(new Asm("JUMP {" + exit + "}"));
	  machineCmds.push_back(new Asm("INC 1", inc));
	  machineCmds.push_back(new Asm("ZERO 0", exit));
  }
}

void Generator::mod(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst) {
  if(fst->name() == Const::klass() && lst->name() == Const::klass()) {
	// Here on the other side AoT
	cln::cl_I val = cln::mod(dynamic_cast<Const*>(fst)->get(),
							 dynamic_cast<Const*>(lst)->get());
	setToNumber(machineCmds, val, 1);
  } else {
	  cln::cl_I ptr;
	  if(lst->name() == Const::klass()) {
		  loadMemToReg(machineCmds, lst, 1);
		  machineCmds.push_back(new Asm("ZERO 0"));
		  machineCmds.push_back(new Asm("STORE 1"));
		  ptr = 0;
	  } else {
		  std::string varName;
		  if(lst->name() == Token::klass()) {
			  varName = dynamic_cast<Token*>(lst)->get();
		  } else {
			  varName = dynamic_cast<TablePtr*>(lst)->getName();
		  }
		  ptr = GEN.findVar(varName)->getPtr();
	  }
	  std::string back = getUniqueIndex();
	  std::string end = getUniqueIndex();
	  std::string zero = getUniqueIndex();
	  std::string exit = getUniqueIndex();
	  loadMemToReg(machineCmds, lst, 1);
	  machineCmds.push_back(new Asm("JZERO 1 {" + exit + "}"));
	  
	  loadMemToReg(machineCmds, fst, 2);
	  loadMemToReg(machineCmds, fst, 1);
	  setToNumber(machineCmds, ptr, 0);
	  machineCmds.push_back(new Asm("ZERO 4", back));
	  machineCmds.push_back(new Asm("SUB 2"));
	  machineCmds.push_back(new Asm("JZERO 2 {" + end + "}"));
	  machineCmds.push_back(new Asm("SUB 1"));
	  machineCmds.push_back(new Asm("JUMP {" + back + "}"));
	  machineCmds.push_back(new Asm("ZERO 0", end));
	  loadMemToReg(machineCmds, lst, 2);
	  machineCmds.push_back(new Asm("ZERO 0"));
	  machineCmds.push_back(new Asm("STORE 1"));
	  machineCmds.push_back(new Asm("SUB 2"));
	  machineCmds.push_back(new Asm("JZERO 2 {" + zero + "}"));
	  machineCmds.push_back(new Asm("JUMP {" + exit + "}"));
	  machineCmds.push_back(new Asm("ZERO 1", zero));
	  machineCmds.push_back(new Asm("ZERO 0", exit));
  }
}

void Generator::eq(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst, std::string &then, std::string &othw) {
	if(fst->name() == Const::klass() && lst->name() == Const::klass()) {
		Const *c1 = dynamic_cast<Const*>(fst);
		Const *c2 = dynamic_cast<Const*>(lst);
		
		if(c1->get() == c2->get()) {
			machineCmds.push_back(new Asm("JUMP {" + then + "}"));
		} else {
			machineCmds.push_back(new Asm("JUMP {" + othw + "}"));
		}
	} else {
		if(fst->name() == Const::klass()) {
			loadMemToReg(machineCmds, fst, 1);
			machineCmds.push_back(new Asm("ZERO 0"));
			machineCmds.push_back(new Asm("STORE 1"));
		} else if(lst->name() == Const::klass()) {
			loadMemToReg(machineCmds, lst, 1);
			machineCmds.push_back(new Asm("ZERO 0"));
			machineCmds.push_back(new Asm("STORE 1"));
		}
		std::string next = getUniqueIndex();
		loadMemToReg(machineCmds, fst, 1);
		loadPtrToReg(machineCmds, lst, 0);
		machineCmds.push_back(new Asm("SUB 1"));
		machineCmds.push_back(new Asm("JZERO 1 {" + next + "}"));
		machineCmds.push_back(new Asm("JUMP {" + othw + "}"));
		machineCmds.push_back(new Asm("ZERO 0", next));
		loadMemToReg(machineCmds, lst, 1);
		loadPtrToReg(machineCmds, fst, 0);
		machineCmds.push_back(new Asm("SUB 1"));
		machineCmds.push_back(new Asm("JZERO 1 {" + then + "}"));
		machineCmds.push_back(new Asm("JUMP {" + othw + "}"));
	}
}

void Generator::neq(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst, std::string &then, std::string &othw) {
	if(fst->name() == Const::klass() && lst->name() == Const::klass()) {
		Const *c1 = dynamic_cast<Const*>(fst);
		Const *c2 = dynamic_cast<Const*>(lst);
		
		if(c1->get() != c2->get()) {
			machineCmds.push_back(new Asm("JUMP {" + then + "}"));
		} else {
			machineCmds.push_back(new Asm("JUMP {" + othw + "}"));
		}
	} else {
		if(fst->name() == Const::klass()) {
			loadMemToReg(machineCmds, fst, 1);
			machineCmds.push_back(new Asm("ZERO 0"));
			machineCmds.push_back(new Asm("STORE 1"));
		} else if(lst->name() == Const::klass()) {
			loadMemToReg(machineCmds, lst, 1);
			machineCmds.push_back(new Asm("ZERO 0"));
			machineCmds.push_back(new Asm("STORE 1"));
		}
		std::string next = getUniqueIndex();
		loadMemToReg(machineCmds, fst, 1);
		loadPtrToReg(machineCmds, lst, 0);
		machineCmds.push_back(new Asm("SUB 1"));
		machineCmds.push_back(new Asm("JZERO 1 {" + next + "}"));
		machineCmds.push_back(new Asm("JUMP {" + then + "}"));
		machineCmds.push_back(new Asm("ZERO 0", next));
		loadMemToReg(machineCmds, lst, 1);
		loadPtrToReg(machineCmds, fst, 0);
		machineCmds.push_back(new Asm("SUB 1"));
		machineCmds.push_back(new Asm("JZERO 1 {" + othw + "}"));
		machineCmds.push_back(new Asm("JUMP {" + then + "}"));
	}
}

void Generator::lt(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst, std::string &then, std::string &othw) {
	if(fst->name() == Const::klass() && lst->name() == Const::klass()) {
		Const *c1 = dynamic_cast<Const*>(fst);
		Const *c2 = dynamic_cast<Const*>(lst);
		
		if(c1->get() < c2->get()) {
			machineCmds.push_back(new Asm("JUMP {" + then + "}"));
		} else {
			machineCmds.push_back(new Asm("JUMP {" + othw + "}"));
		}
	} else {
		if(fst->name() == Const::klass()) {
			loadMemToReg(machineCmds, fst, 1);
			machineCmds.push_back(new Asm("ZERO 0"));
			machineCmds.push_back(new Asm("STORE 1"));
		} else if(lst->name() == Const::klass()) {
			loadMemToReg(machineCmds, lst, 1);
			machineCmds.push_back(new Asm("ZERO 0"));
			machineCmds.push_back(new Asm("STORE 1"));
		}
		std::string next = getUniqueIndex();
		loadMemToReg(machineCmds, fst, 1);
		loadPtrToReg(machineCmds, lst, 0);
		machineCmds.push_back(new Asm("SUB 1"));
		machineCmds.push_back(new Asm("JZERO 1 {" + next + "}"));
		machineCmds.push_back(new Asm("JUMP {" + othw + "}"));
		machineCmds.push_back(new Asm("ZERO 0", next));
		loadMemToReg(machineCmds, lst, 1);
		loadPtrToReg(machineCmds, fst, 0);
		machineCmds.push_back(new Asm("SUB 1"));
		machineCmds.push_back(new Asm("JZERO 1 {" + othw + "}"));
		machineCmds.push_back(new Asm("JUMP {" + then + "}"));
	}
}

void Generator::gt(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst, std::string &then, std::string &othw) {
	if(fst->name() == Const::klass() && lst->name() == Const::klass()) {
		Const *c1 = dynamic_cast<Const*>(fst);
		Const *c2 = dynamic_cast<Const*>(lst);
		
		if(c1->get() > c2->get()) {
			machineCmds.push_back(new Asm("JUMP {" + then + "}"));
		} else {
			machineCmds.push_back(new Asm("JUMP {" + othw + "}"));
		}
	} else {
		if(fst->name() == Const::klass()) {
			loadMemToReg(machineCmds, fst, 1);
			machineCmds.push_back(new Asm("ZERO 0"));
			machineCmds.push_back(new Asm("STORE 1"));
		} else if(lst->name() == Const::klass()) {
			loadMemToReg(machineCmds, lst, 1);
			machineCmds.push_back(new Asm("ZERO 0"));
			machineCmds.push_back(new Asm("STORE 1"));
		}
		std::string next = getUniqueIndex();
		loadMemToReg(machineCmds, fst, 1);
		loadPtrToReg(machineCmds, lst, 0);
		machineCmds.push_back(new Asm("SUB 1"));
		machineCmds.push_back(new Asm("JZERO 1 {" + othw + "}"));
		machineCmds.push_back(new Asm("JUMP {" + then + "}"));
	}
}

void Generator::leq(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst, std::string &then, std::string &othw) {
	if(fst->name() == Const::klass() && lst->name() == Const::klass()) {
		Const *c1 = dynamic_cast<Const*>(fst);
		Const *c2 = dynamic_cast<Const*>(lst);
		
		if(c1->get() <= c2->get()) {
			machineCmds.push_back(new Asm("JUMP {" + then + "}"));
		} else {
			machineCmds.push_back(new Asm("JUMP {" + othw + "}"));
		}
	} else {
		if(fst->name() == Const::klass()) {
			loadMemToReg(machineCmds, fst, 1);
			machineCmds.push_back(new Asm("ZERO 0"));
			machineCmds.push_back(new Asm("STORE 1"));
		} else if(lst->name() == Const::klass()) {
			loadMemToReg(machineCmds, lst, 1);
			machineCmds.push_back(new Asm("ZERO 0"));
			machineCmds.push_back(new Asm("STORE 1"));
		}
		std::string next = getUniqueIndex();
		loadMemToReg(machineCmds, fst, 1);
		loadPtrToReg(machineCmds, lst, 0);
		machineCmds.push_back(new Asm("SUB 1"));
		machineCmds.push_back(new Asm("JZERO 1 {" + then + "}"));
		machineCmds.push_back(new Asm("JUMP {" + othw + "}"));
	}
}

void Generator::geq(std::list<Asm*> &machineCmds, Ref *fst, Ref *lst, std::string &then, std::string &othw) {
	if(fst->name() == Const::klass() && lst->name() == Const::klass()) {
		Const *c1 = dynamic_cast<Const*>(fst);
		Const *c2 = dynamic_cast<Const*>(lst);
		
		if(c1->get() >= c2->get()) {
			machineCmds.push_back(new Asm("JUMP {" + then + "}"));
		} else {
			machineCmds.push_back(new Asm("JUMP {" + othw + "}"));
		}
	} else {
				if(fst->name() == Const::klass()) {
			loadMemToReg(machineCmds, fst, 1);
			machineCmds.push_back(new Asm("ZERO 0"));
			machineCmds.push_back(new Asm("STORE 1"));
		} else if(lst->name() == Const::klass()) {
			loadMemToReg(machineCmds, lst, 1);
			machineCmds.push_back(new Asm("ZERO 0"));
			machineCmds.push_back(new Asm("STORE 1"));
		}
		std::string next = getUniqueIndex();
		loadMemToReg(machineCmds, fst, 1);
		loadPtrToReg(machineCmds, lst, 0);
		machineCmds.push_back(new Asm("SUB 1"));
		machineCmds.push_back(new Asm("JZERO 1 {" + next + "}"));
		machineCmds.push_back(new Asm("JUMP {" + then + "}"));
		machineCmds.push_back(new Asm("ZERO 0", next));
		loadMemToReg(machineCmds, lst, 1);
		loadPtrToReg(machineCmds, fst, 0);
		machineCmds.push_back(new Asm("SUB 1"));
		machineCmds.push_back(new Asm("JZERO 1 {" + then + "}"));
		machineCmds.push_back(new Asm("JUMP {" + othw + "}"));
	}
}
