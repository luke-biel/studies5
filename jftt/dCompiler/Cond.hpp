#ifndef COND_HPP
#define COND_HPP

#include "Ref.hpp"
#include "Util.hpp"

class Cond {
  KLASS(Cond);
protected:
  Ref *m_fst, *m_snd;
  
public:
  Cond(Ref *fst, Ref *snd) :
    m_fst(fst),
    m_snd(snd) { }

  virtual ~Cond() {delete m_fst; delete m_snd; }

  Ref *fst() { return m_fst; }
  Ref *snd() { return m_snd; }
};

class Eq : public Cond {
  KLASS(Eq);
public:
  Eq(Ref *fst, Ref *snd) :
    Cond(fst, snd) { }

  virtual ~Eq() { }
};

class Neq : public Cond {
  KLASS(Neq);
public:
  Neq(Ref *fst, Ref *snd) :
    Cond(fst, snd) { }

  virtual ~Neq() { }
};

class Lt : public Cond {
  KLASS(Lt);
public:
  Lt(Ref *fst, Ref *snd) :
    Cond(fst, snd) { }

  virtual ~Lt() { }
};

class Gt : public Cond {
  KLASS(Gt);
public:
  Gt(Ref *fst, Ref *snd) :
    Cond(fst, snd) { }

  virtual ~Gt() { }
};

class Leq : public Cond {
  KLASS(Leq);
public:
  Leq(Ref *fst, Ref *snd) :
    Cond(fst, snd) { }

  virtual ~Leq() { }
};

class Geq : public Cond {
  KLASS(Geq);
public:
  Geq(Ref *fst, Ref *snd) :
    Cond(fst, snd) { }

  virtual ~Geq() { }
};

#endif
