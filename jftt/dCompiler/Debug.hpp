#ifndef DEBUG_HPP
#define DEBUG_HPP

#include <iostream>

#if YYDEBUG == 1
#define DEBUG(x) do {std::cerr << x << std::endl;} while(0)
#else
#define DEBUG(x) do {} while(0)
#endif

#endif
