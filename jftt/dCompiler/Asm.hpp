#ifndef ASM_HPP
#define ASM_HPP

#include <string>

class Asm {
private:
  std::string m_cmd;
  std::string m_label;

public:
  Asm(std::string cmd, std::string label = "") :
    m_cmd(cmd),
    m_label(label) { }

  virtual ~Asm() { }

  std::string getCmd() { return m_cmd; }
  std::string getLabel() { return m_label; }
};

#endif
