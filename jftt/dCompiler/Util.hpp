#ifndef UTIL_HPP
#define UTIL_HPP

#include <string>
#include <list>

#define KLASS(x)					\
  public:						\
  virtual const std::string name() { return #x; }	\
  static const std::string klass() { return #x; }	\
  private:


#endif
