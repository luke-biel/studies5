#ifndef COMMAND_HPP
#define COMMAND_HPP

#include <list>

#include "Var.hpp"
#include "Ref.hpp"
#include "Exp.hpp"
#include "Cond.hpp"
#include "Util.hpp"

class Command {
  KLASS(Command);
public:
  Command() { }
  virtual ~Command() { }

  virtual void translate(std::list<Asm*> &machineCmds) = 0;
};

class Commands {
  KLASS(Commands);
private:
  std::list<Command*> m_commands;

public:
  Commands() { }

  void add(Command *cmd);
  std::list<Command*>::iterator begin();
  std::list<Command*>::iterator end();
  
  virtual ~Commands() {
    for(auto cmd : m_commands) {
      delete cmd;
    }
  }
};

class Assign : public Command {
  KLASS(Assign);
private:
  Ref *m_where;
  Exp *m_what;
public:
  Assign(Ref *where, Exp *what) :
    m_where(where),
    m_what(what) { }

  virtual ~Assign() { delete m_where; delete m_what; }

  virtual void translate(std::list<Asm*> &machineCmds);
};

class If : public Command {
  KLASS(If);
private:
  Cond *m_cond;
  Commands *m_then, *m_othw;
public:
  If(Cond *cond, Commands *then, Commands *othw) :
    m_cond(cond),
    m_then(then),
    m_othw(othw) { }

  Commands *getThen() { return m_then; }
  Commands *getElse() { return m_othw; }

  virtual ~If() { delete m_cond; delete m_then; delete m_othw; }

  virtual void translate(std::list<Asm*> &machineCmds);
};

class While : public Command {
  KLASS(While);
private:
  Cond *m_cond;
  Commands *m_then;
public:
  While(Cond *cond, Commands *then) :
    m_cond(cond),
    m_then(then) { }

  Commands *getThen() { return m_then; }
  
  virtual ~While() {
    delete m_cond;
    delete m_then;
  }

  virtual void translate(std::list<Asm*> &machineCmds);
};

class ForUp : public Command {
  KLASS(ForUp);
private:
  Iterator *m_it;
  Commands *m_then;
public:
  ForUp(std::string name, Ref *min, Ref *max, Commands *then) :
    m_it(new Iterator(name, min, max)),
    m_then(then) { }

  Commands *getThen() { return m_then; }

  virtual ~ForUp() { delete m_it; delete m_then; }

  virtual void translate(std::list<Asm*> &machineCmds);
};

class ForDown : public Command {
  KLASS(ForUp);
private:
  Iterator *m_it;
  Commands *m_then;
public:
  ForDown(std::string name, Ref *min, Ref *max, Commands *then) :
    m_it(new Iterator(name, min, max)),
    m_then(then) { }

  Commands *getThen() { return m_then; }

  virtual ~ForDown() { delete m_it; delete m_then; }

  virtual void translate(std::list<Asm*> &machineCmds);
};

class Read : public Command {
  KLASS(Read);
private:
  Ref *m_id;
public:
  Read(Ref *id) :
    m_id(id) { }

  virtual ~Read() { delete m_id; }

  virtual void translate(std::list<Asm*> &machineCmds);
};

class Write : public Command {
  KLASS(Write);
private:
  Ref *m_val;
public:
  Write(Ref *val) :
    m_val(val) { }

  virtual ~Write() { delete m_val; }

  virtual void translate(std::list<Asm*> &machineCmds);
};

class Skip : public Command {
  KLASS(Skip);
public:
  Skip() { }

  virtual ~Skip() { }

  virtual void translate(std::list<Asm*> &machineCmds);
};

#endif
