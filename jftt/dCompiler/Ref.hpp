#ifndef REF_HPP
#define REF_HPP

#include <cln/cln.h>
#include <list>

#include "Util.hpp"
#include "Asm.hpp"

class Ref {
  KLASS(Ref);
public:
  Ref() {}
  virtual ~Ref() { }
};

class Const : public Ref {
  KLASS(CONST);
private:
  cln::cl_I m_num;

public:
  Const(cln::cl_I num) :
    m_num(num) { }
  virtual ~Const() { }

  cln::cl_I get() { return m_num; }
};

class Token : public Ref {
  KLASS(Token);
private:
  std::string m_name;

public:
  Token(std::string name) :
    m_name(name) { }
  virtual ~Token() { }

  std::string get() { return m_name; }
};

class TablePtr : public Ref {
  KLASS(TablePtr);
private:
  std::string m_name;
  Ref *m_at;
  
public:
  TablePtr(std::string name, Ref *at) :
    m_name(name),
    m_at(at) { }

  virtual ~TablePtr() { delete m_at; }

  std::string getName() { return m_name; }
  Ref *getPtr() { return m_at; }
};

#endif
