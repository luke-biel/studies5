#ifndef VAR_HPP
#define VAR_HPP

#include <cln/cln.h>

#include "Debug.hpp"
#include "Ref.hpp"
#include "Util.hpp"

class Var {
  KLASS(Var);
private:
  std::string m_name;
  cln::cl_I m_ptr;

protected:
  static cln::cl_I s_count;

public:
  static cln::cl_I next();
  
  Var(std::string name) :
    m_name(name),
    m_ptr(next()) {
    DEBUG("Registering new var(" << name << ", " << m_ptr << ')');
  }

  std::string getName() { return m_name; }
  cln::cl_I getPtr() { return m_ptr; }
  
  virtual ~Var() { }  
};

class Integer : public Var {
  KLASS(Integer);
public:
  Integer(std::string name) : Var(name) { }

  virtual ~Integer() { }
};

class Table : public Var {
  KLASS(Table);
private:
  cln::cl_I m_size;

public:
  Table(std::string name, cln::cl_I size) :
    Var(name),
    m_size(size) { s_count += size; }

  virtual ~Table() { }

  cln::cl_I getSize() { return m_size; }
};

class Iterator : public Var {
  KLASS(Iterator);
private:
  Ref *m_min, *m_max;

public:
  Iterator(std::string name, Ref *min, Ref *max) :
    Var(name),
    m_min(min),
    m_max(max) { }

  virtual ~Iterator() { delete m_min; delete m_max; }

	Ref *getMin() { return m_min; }
	Ref *getMax() { return m_max; }
};

#endif
