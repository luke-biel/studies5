%{
#include"y.tab.h"
extern int yylval;
%}
%%
[ \t]+
(-)?[0-9]+  {yylval=atoi(yytext); return NUM;}
\+      {return ADD;}
-       {return SUB;}
\*      {return MUL;}
\/      {return DIV;}
\%      {return MOD;}
\^      {return POW;}
\(      {return LBR;}
\)      {return RBR;}
\n      {return CMPT;}
^#.*\n  {return COMM;}      
%%

int yywrap(){
    return 1;
}
