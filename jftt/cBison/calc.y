%{
#include<stdio.h>
#include <math.h>

  extern int yyerror(char*);
  extern int yylex(void);
  extern int yylval;
  bool error;
%}

%token CMPT COMM
%token NUM
%token ADD SUB MUL DIV
%token MOD POW
%token LBR RBR

%left ADD SUB
%left MUL DIV
%left MOD
%right POW
%right NEG

%%
stmt   :   stmt expr CMPT      {putchar('\n'); if(!error) printf("== %d\n", $2); error = false;}
       |   stmt COMM
       |   error '\n'          {yyerrok; $$ = 1;}
       |   
       ;
expr   :   expr ADD expr       {printf("+ "); $$ = $1 + $3;}
       |   expr SUB expr       {printf("- "); $$ = $1 - $3;}
       |   expr MUL expr       {printf("* "); $$ = $1 * $3;}
       |   expr DIV expr       {printf("/ "); if($3 == 0) {fprintf(stderr, "Cannot divide by 0! "); error = true;} else {$$ = $1 / $3;}}
       |   expr MOD expr       {printf("%% "); $$ = $1 % $3;}
       |   expr POW expr       {printf("^ "); $$ = pow($1, $3);}
       |   LBR expr RBR        {$$ = $2;}
       |   SUB expr %prec NEG  {$$ = -$2;}
       |   NUM                 {printf("%d ", yylval); $$ = $1;}
       ;
%%

int main(){
  error = false;
  yyparse();
}

int yyerror (char *msg) {
    return printf ("error YACC: %s\n", msg);
}
