%{
#include <stdbool.h>
%}

    bool multiline = false;

%%
.               if(!multiline) printf("%s", yytext);
\/\*            multiline = true;
\*\/            multiline = false;
\/\/.*          /* ignore this token */

%%


main(argc, argv)
int argc;
char** argv;
{
  if(argc > 1)
    yyin = fopen(argv[1], "r");
  else
    yyin = stdin;
  yylex();
}
