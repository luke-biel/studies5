%{
#include <math.h>
#include <stdbool.h>
%}

    int* acc;
    int pc = 0;
    bool fail = false;

%%

[ \t]
(\-)?[0-9]+   acc[pc++] = atoi(yytext);
\+            if(pc - 2 >= 0) { acc[pc - 2] = acc[pc - 2] + acc[pc - 1]; pc -= 1; } else { printf("Invalid number of arguments\n"); fail = true; }
-             if(pc - 2 >= 0) { acc[pc - 2] = acc[pc - 2] - acc[pc - 1]; pc -= 1; } else { printf("Invalid number of arguments\n"); fail = true; }
\*            if(pc - 2 >= 0) { acc[pc - 2] = acc[pc - 2] * acc[pc - 1]; pc -= 1; } else { printf("Invalid number of arguments\n"); fail = true; }
\/            if(acc[pc - 1] == 0) {printf("Cant divide by 0, "); fail = true; } else if(pc - 2 >= 0) { acc[pc - 2] = acc[pc - 2] / acc[pc - 1]; pc -= 1; } else { printf("Invalid number of arguments\n"); fail = true; }
\^            if(pc - 2 >= 0) { acc[pc - 2] = (int)pow((double)acc[pc - 2], (double)acc[pc - 1]); pc -= 1; } else { printf("Invalid number of arguments\n"); fail = true; }
\%            if(pc - 2 >= 0) { acc[pc - 2] = acc[pc - 2] % acc[pc - 1]; pc -= 1; } else { printf("Invalid number of arguments\n"); fail = true; }
[\n<<eof>>]   if(pc != 1) {printf("Invalid number of arguments\n"); } else if(!fail) { printf("= %d\n", acc[0]); } pc = 0; fail = false;
.             printf("Invalid symbol\n"); pc = 0; fail = true;

%%


main(argc, argv)
int argc;
char** argv;
{
  if(argc > 1)
    yyin = fopen(argv[1], "r");
  else
    yyin = stdin;
  acc = calloc(4096, sizeof(int));
  yylex();
}
