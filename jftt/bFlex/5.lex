%{
#include <stdbool.h>
%}

    bool isSingleComment = 0, isMultiComment = 0, isString = 0;

%%

\"        isString = !isString; putchar('"');
\\\"      printf("\\\*");
\/\/      if(isString) printf("%s", yytext); else isSingleComment = true;
\\\n      if(isString) printf("%s", yytext); else if(!isSingleComment) isSingleComment = true; printf("\n");
\n        isSingleComment = false; putchar('\n');
.         if(!isSingleComment && !isMultiComment) printf("%s", yytext);
\/\*      if(isString) printf("%s", yytext); else if(!isSingleComment) isMultiComment = true;
\*\/      if(isString) printf("%s", yytext); else if(!isSingleComment) isMultiComment = false;
    
%%

main(argc, argv)
int argc;
char** argv;
{
  if(argc > 1)
    yyin = fopen(argv[1], "r");
  else
    yyin = stdin;
  yylex();
}
