	int num_lines = 0, num_words = 0;

%%
[ \t]+$                     /* ignore this token */
^[ \t]+                     /* ignore this token */
^\n                   /* ignore this token */
\n       	            putchar('\n'); ++num_lines;
<EOF>                       ++num_lines;
[[:alnum:]]+ 	            printf("%s", yytext); ++num_words;
[ \t]+                      putchar( ' ' );
%%

main(argc, argv)
int argc;
char** argv;
{
  if(argc > 1)
    yyin = fopen(argv[1], "r");
  else
    yyin = stdin;
  yylex();
  printf("\n# of lines = %d, # of words = %d\n", num_lines, num_words);
}
