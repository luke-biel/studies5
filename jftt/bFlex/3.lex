%{
#include <stdbool.h>
%}

    bool multiline = false, documentation = false;
    char* macc;
    
%%
[ \t]*\/\*\*            strcat(macc, yytext); documentation = true; multiline = true;
\/\*                    multiline = true;
\*\/                    if(documentation) strcat(macc, yytext); multiline = false; documentation = false;
\/\/.*                  /* ignore this token */
.                       {
                         if(!multiline && !documentation && *macc != 0) {
		          printf("%s\n", macc);
		          *macc = 0;
                         }
                         if(!multiline && !documentation) printf("%s", yytext);
		         if(documentation) strcat(macc, yytext);
                        }
\n                      if(documentation) strcat(macc, "\n"); else putchar('\n');
%%

main(argc, argv)
int argc;
char** argv;
{
  if(argc > 1)
    yyin = fopen(argv[1], "r");
  else
    yyin = stdin;
  macc = calloc(4096, sizeof(char));
  yylex();
}
