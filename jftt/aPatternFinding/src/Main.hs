module Main where

import Data.Map.Strict (empty, union, singleton, (!), Map)
import Data.List (isSuffixOf)
import System.Environment

{-
Finite-Automaton-Matcher
-}
fam :: [Char] -> Map (Int, Char) Int -> Int -> Maybe Int
fam = fam' 0 0

fam' :: Int -> Int -> [Char] -> Map (Int, Char) Int -> Int ->  Maybe Int
fam' q i t d m =
  if i == n
  then Nothing
  else if qx == m
       then Just ((i-m) + 1) -- +1 because we start counting from 0
       else fam' qx (i+1) t d m
  where
    qx = d ! (q, (t !! i))
    n = length t

{-
Compute-Transition-Function
-}
ctf :: [Char] -> [Char] -> Map (Int, Char) Int
ctf p a = foldl1 union (map (ctf' p) a)

ctf' :: [Char] -> Char -> Map (Int, Char) Int
ctf' p c =
  foldl1 union $ map (\i -> singleton (i, c) (k l c i p (k1 i))) [0..l-1]
  where
    l = length p
    k1 q = min l (q+1)
    k m a q pat 0 = 0
    k m a q pat i = if pi `isSuffixOf` pqa
           then i
           else k m a q pat (i-1)
                where
                  pqa = (take q pat) ++ [a]
                  pi = take i pat


automata :: [Char] -> [Char] -> [Char] -> Maybe Int
automata pat alp hay = fam hay c l
  where
    c = ctf pat alp
    l = length pat

main :: IO ()
main = do
  args <- getArgs
  putStrLn $ show $ automata (args !! 0) (args !! 1) (args !! 2)
