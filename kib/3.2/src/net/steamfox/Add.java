package net.steamfox;

import net.sourceforge.argparse4j.inf.Namespace;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;

public class Add extends PlayerAction {

    String file;

    @Override
    void handle(Namespace ns) {
        file = ns.getString("file");
        try {
            FileCrypto.convertFile(
                    Main.config.keystoreFile,
                    Main.config.index,
                    Main.config.keystorePassword,
                    Main.config.keyPassword,
                    "AES",
                    "CBC",
                    Cipher.ENCRYPT_MODE,
                    file,
                    file + ".cp");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        System.out.println("File added!");
    }
}
