package net.steamfox;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;
import javazoom.jl.player.advanced.AdvancedPlayer;
import net.sourceforge.argparse4j.inf.Namespace;

import javax.crypto.Cipher;
import javax.sound.sampled.*;
import java.io.*;
import java.net.URL;

public class Play extends PlayerAction {

    String file;
    double start;

    @Override
    void handle(Namespace ns) {
        file = ns.getString("file");
        start = ns.getDouble("start");

        String playFilename = "/tmp/" + file;
        playFilename = playFilename.replace(".cp", "");

        try {
            FileCrypto.convertFile(
                    Main.config.keystoreFile,
                    Main.config.index,
                    Main.config.keystorePassword,
                    Main.config.keyPassword,
                    "AES",
                    "CBC",
                    Cipher.DECRYPT_MODE,
                    file,
                    playFilename);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

        Player player = null;

        try {
            FileInputStream fis     = new FileInputStream(playFilename);
            BufferedInputStream bis = new BufferedInputStream(fis);
            player = new Player(bis);
        }
        catch (Exception e) {
            System.out.println("Problem playing file " + playFilename);
            System.out.println(e);
        }

        // run in new thread to play in background
        Player finalPlayer = player;
        new Thread() {
            public void run() {
                try { System.out.println("Playing..."); finalPlayer.play(); }
                catch (Exception e) { System.out.println(e); }
            }
        }.start();

    }
}
