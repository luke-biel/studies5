package net.steamfox;

import net.sourceforge.argparse4j.inf.Namespace;

/**
 * Created by dot on 01.02.17.
 */
public abstract class PlayerAction {
    abstract void handle(Namespace ns);
}

