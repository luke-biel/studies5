package net.steamfox;

import com.google.gson.Gson;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparser;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

/**
 * Created by dot on 01.02.17.
 */
public class Main {
    static Key configKey = new SecretKeySpec("43BA08B3AF34AD2C6B8B3CAF201F5676".getBytes(), "AES");
    static String configFilename = "config.json";
    static File configFile;
    static Config config;

    public static void install() throws
            NoSuchPaddingException,
            InvalidAlgorithmParameterException,
            NoSuchAlgorithmException,
            IOException,
            BadPaddingException,
            IllegalBlockSizeException,
            InvalidKeyException {
        config = new Config();
        Console c = System.console();
        c.printf("Please specify path to keystore...\n");
        config.keystoreFile = c.readLine();
        c.printf("Please specify key index...\n");
        config.index = c.readLine();
        c.printf("Please specify keystore password...\n");
        config.keyPassword = c.readPassword();
        c.printf("Please specify key password...\n");
        config.keyPassword = c.readPassword();
        Gson g = new Gson();
        String json = g.toJson(config);
        FileCrypto.convertString(configKey, "AES", "CBC", Cipher.ENCRYPT_MODE, json, configFilename);
        c.printf("Installed\n");
    }

    public static void loadConfig() throws
            IOException,
            NoSuchPaddingException,
            InvalidKeyException,
            NoSuchAlgorithmException,
            IllegalBlockSizeException,
            BadPaddingException,
            InvalidAlgorithmParameterException {
        configFile = new File(configFilename);
        Gson g = new Gson();
        if(!configFile.exists()) {
            install();
            return;
        }

        config = g.fromJson(
                FileCrypto.convertToString(configKey, "AES", "CBC", Cipher.DECRYPT_MODE, configFilename),
                Config.class);
    }

    public static void main(String[] args) throws
            IOException,
            NoSuchPaddingException,
            InvalidAlgorithmParameterException,
            NoSuchAlgorithmException,
            IllegalBlockSizeException,
            BadPaddingException,
            InvalidKeyException {
        ArgumentParser parser = ArgumentParsers.newArgumentParser("MP3")
                .defaultHelp(true)
                .description("MP3 player for encrypted files!");
        Subparser add = parser.addSubparsers().addParser("add")
                .setDefault("func", new Add());
        add.addArgument("-f", "--file")
                .help("Path to file to be added!");
        Subparser play = parser.addSubparsers().addParser("play")
                .setDefault("func", new Play());
        play.addArgument("-f", "--file")
                .help("Path to file to be played!");
        play.addArgument("-s", "--start")
                .setDefault(0.0)
                .help("Track starting point!");

        loadConfig();

        Namespace ns = null;

        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }

        ((PlayerAction)ns.get("func")).handle(ns);
    }
}
