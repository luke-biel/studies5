module Crypto.Dot (run, runFragmented, runMultithreaded) where

import           Control.Concurrent
import           Control.Parallel.Strategies
import           Crypto.Cipher
import           Crypto.Cipher.Types
import qualified Data.ByteString.Base16 as BS16
import qualified Data.ByteString.Base64 as BS64
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString.UTF8 as UTF8
import           Data.IORef
import           Numeric

prepareKey :: String -> Either KeyError (Key AES256)
prepareKey = makeKey . fst . BS16.decode . BS.pack

prepareCrypto :: Key AES256 -> AES256
prepareCrypto = cipherInit

prepareIV :: String -> Maybe (IV AES256)
prepareIV = makeIV . fst . BS16.decode . BS.pack

prepare :: String -> String -> Either String (AES256, IV AES256)
prepare key iv = prepare' eitherKey maybeIV
  where
    maybeIV = prepareIV iv
    eitherKey = prepareKey key

prepare' :: Either KeyError (Key AES256) -> Maybe (IV AES256) ->
  Either [Char] (AES256, IV AES256)
prepare' (Left key) _ = Left $ case key of
                                 KeyErrorTooSmall -> "Invalid key!"
                                 KeyErrorTooBig -> "Too big!"
                                 (KeyErrorInvalid msg) -> "Invalid key! " ++ msg
prepare' _ Nothing = Left "Invalid IV!"
prepare' (Right key) (Just iv) = Right ((prepareCrypto key), iv)

runFragmented :: String -> String -> String -> String
runFragmented key iv cipher
  | ln == 64 = run key iv cipher
  | ln < 64 =
    let
      bs = map (\n -> runFragmented (showHex n key) iv cipher) [0..15]
      cs = bs `using` parList rdeepseq
      in
      concat $ filter (all (/= '\xFFFD')) cs
  | otherwise = "Too long key! " ++ key ++ "\n"
  where
    ln = length key

runMultithreaded :: String -> String -> String -> (IORef String) -> IO ()
runMultithreaded key iv cipher r
  | ln == 64 = do
      forkIO $ runThread key iv cipher r
      return ()
  | ln < 64 = do
      result <- readIORef r
      if length result == 0
        then do
        mapM (\n -> runMultithreaded (showHex n key) iv cipher r) [0..15]
        return ()
        else return ()
   | otherwise = do
       atomicWriteIORef r "Too long key!"
       return ()
         where ln = length key

runThread :: String -> String -> String -> (IORef String) -> IO ()
runThread key iv cipher r = do
  result <- readIORef r
  if length result == 0 && all (/= '\xFFFD') resolved
    then do
    atomicWriteIORef r (resolved ++ ", good key was: " ++ key)
    return ()
    else return ()
    where
      resolved = run key iv cipher   

run :: String -> String -> String -> String
run key iv cipher =
  case BS64.decode $ BS.pack cipher of
    (Right text) -> case run' keychain text of
                      (Right success) -> UTF8.toString success
                      (Left runFailure) -> runFailure
    (Left convertError) -> convertError
  where
    keychain = prepare key iv

run' :: (BlockCipher cipher) =>
  Either String (cipher, IV cipher) -> BS.ByteString -> Either String BS.ByteString
run' (Left msg) _ = Left msg
run' (Right (c, iv)) cipher = Right (cbcDecrypt c iv cipher)
