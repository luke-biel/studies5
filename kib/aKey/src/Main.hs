import           Control.Concurrent
import qualified Crypto.Dot as Decipher
import           Data.IORef
import           Options.Applicative

data Application = Application
  { key :: String
  , iv :: String
  , cipher :: String }

application :: Parser Application
application = Application
  <$> strOption
    (  long "key"
    <> short 'k'
    <> metavar "KEY"
    <> noArgError (ErrorMsg "Key is required to decipher a cipher!") )
  <*> strOption
    ( long "iv"
   <> metavar "IV"
   <> noArgError (ErrorMsg "IV is required to decipher a cipher!") )
  <*> strOption
    ( long "cipher"
   <> short 'c'
   <> metavar "TEXT"
   <> noArgError (ErrorMsg "Program needs input to decipher!") )

decipher :: Application -> IO ()
decipher (Application key iv cipher) = do
  r <- newIORef ""
  Decipher.runMultithreaded key iv cipher r
  waitForResult r

waitForResult :: IORef String -> IO ()
waitForResult r = do
  threadDelay 10000
  try <- readIORef r
  if length try > 0
    then putStrLn try
    else waitForResult r

main :: IO ()
main = execParser opts >>= decipher
  where
    opts = info (helper <*> application)
      ( fullDesc
     <> progDesc "Decrypt a cipher encoded with AES-256-CBC with partial key" )
