var mime_samples = [
  { 'mime': 'text/css', 'samples': [
    { 'url': 'https://dot.steamfox.net/reset.css', 'dir': '_m0/0', 'linked': 2, 'len': 1130 },
    { 'url': 'https://dot.steamfox.net/style.css', 'dir': '_m0/1', 'linked': 2, 'len': 576 } ]
  },
  { 'mime': 'text/html', 'samples': [
    { 'url': 'https://dot.steamfox.net/', 'dir': '_m1/0', 'linked': 2, 'len': 590 },
    { 'url': 'https://dot.steamfox.net/register_submit.php', 'dir': '_m1/1', 'linked': 5, 'len': 317 } ]
  }
];

var issue_samples = [
  { 'severity': 2, 'type': 30203, 'samples': [
    { 'url': 'https://dot.steamfox.net/sfi9876.css', 'extra': 'dot@steamfox.net', 'sid': '0', 'dir': '_i0/0' } ]
  },
  { 'severity': 2, 'type': 30202, 'samples': [
    { 'url': 'https://dot.steamfox.net/sfi9876.css', 'extra': '/C=PL/ST=Dolno\x5cxC3\x5cx85\x5cxC2\x5cx9Bl\x5cxC3\x5cx84\x5cxC2\x5cx85skie/L=Jelenia G\x5cxC3\x5cx83\x5cxC2\x5cxB3ra/O=SteamPoweredDev/OU=development/CN=dot.steamfox.net/emailAddress=dot@steamfox.net', 'sid': '0', 'dir': '_i1/0' } ]
  },
  { 'severity': 1, 'type': 20205, 'samples': [
    { 'url': 'https://dot.steamfox.net/register_submit.php', 'extra': 'Responses too slow for time sensitive tests', 'sid': '0', 'dir': '_i2/0' } ]
  },
  { 'severity': 0, 'type': 10803, 'samples': [
    { 'url': 'https://dot.steamfox.net/reset.css', 'extra': '', 'sid': '0', 'dir': '_i3/0' },
    { 'url': 'https://dot.steamfox.net/style.css', 'extra': '', 'sid': '0', 'dir': '_i3/1' } ]
  },
  { 'severity': 0, 'type': 10602, 'samples': [
    { 'url': 'https://dot.steamfox.net/register_submit.php', 'extra': '', 'sid': '0', 'dir': '_i4/0' } ]
  },
  { 'severity': 0, 'type': 10205, 'samples': [
    { 'url': 'https://dot.steamfox.net/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i5/0' },
    { 'url': 'https://dot.steamfox.net/sfi9876.phps', 'extra': '', 'sid': '0', 'dir': '_i5/1' } ]
  },
  { 'severity': 0, 'type': 10202, 'samples': [
    { 'url': 'https://dot.steamfox.net/', 'extra': 'Apache/2.4.18 (Ubuntu)', 'sid': '0', 'dir': '_i6/0' } ]
  }
];

