package net.steamfox;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.io.*;
import java.security.*;
import java.security.cert.CertificateException;

/**
 * Created by dot on 01.02.17.
 */
public class FileCrypto {
    static byte[] ivAES = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    static byte[] ivDES = { 0, 0, 0, 0, 0, 0, 0, 0 };
    static IvParameterSpec ivAESspec = new IvParameterSpec(ivAES);
    static IvParameterSpec ivDESspec = new IvParameterSpec(ivDES);

    public static void convertFile(String keystorePath,
                                   String keyIndex,
                                   char[] keystorePassword,
                                   char[] keyPassword,
                                   String scheme,
                                   String encryptionMode,
                                   int mode,
                                   String fileIn,
                                   String fileOut) throws
            IOException,
            KeyStoreException,
            CertificateException,
            NoSuchAlgorithmException,
            UnrecoverableKeyException,
            NoSuchPaddingException,
            InvalidAlgorithmParameterException,
            InvalidKeyException,
            BadPaddingException,
            IllegalBlockSizeException {

        FileInputStream kis = new FileInputStream(keystorePath);
        KeyStore ks = KeyStore.getInstance("JCEKS");
        ks.load(kis, keystorePassword);
        Key key = ks.getKey(keyIndex, keyPassword);
        kis.close();

        StringBuilder instance = new StringBuilder();
        instance.append(scheme);
        instance.append("/");
        instance.append(encryptionMode);
        instance.append("/PKCS5Padding");
        Cipher c = Cipher.getInstance(instance.toString());

        IvParameterSpec ivSpec = null;
        if(!encryptionMode.equals("ECB")) {
            switch (scheme) {
                case "AES":
                    ivSpec = ivAESspec;
                    break;
                case "DES":
                    ivSpec = ivDESspec;
                    break;
            }
            c.init(mode, key, ivSpec);
        } else {
            c.init(mode, key);
        }

        File f = new File(fileIn);
        FileInputStream fis = new FileInputStream(f);
        FileOutputStream fos = new FileOutputStream(fileOut);

        byte[] buf = new byte[(int)f.length()];
        fis.read(buf);
        byte[] out = c.doFinal(buf);

        fos.write(out);

        fos.close();
        fis.close();
    }
}
