package net.steamfox;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.io.*;
import java.security.*;
import java.security.cert.CertificateException;

public class Main {

    /*
    Create keystore:
    keytool -genseckey -keystore aes-keystore.jck -storetype jceks -keyalg AES -keysize 256 -alias locker
     */
    public static void main(String[] args) throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, InvalidAlgorithmParameterException {
        byte[] ivAES = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        byte[] ivDES = { 0, 0, 0, 0, 0, 0, 0, 0 };
        IvParameterSpec ivAESspec = new IvParameterSpec(ivAES);
        IvParameterSpec ivDESspec = new IvParameterSpec(ivDES);

        ArgumentParser parser = ArgumentParsers.newArgumentParser("Encryptor/Decryptor")
                .defaultHelp(false)
                .description("Encrypt or decrypt given file");
        parser.addArgument("-m", "--mode")
                .choices("d", "e").setDefault("e")
                .help("Select mode to work in");
        parser.addArgument("-o", "--fileOut")
                .help("File to be encrypted or decrypted");
        parser.addArgument("-i", "--fileIn")
                .help("File to be encrypted or decrypted");
        parser.addArgument("-s", "--scheme")
                .choices("AES", "DES").setDefault("AES")
                .help("Scheme of encryption algorithm to use");
        parser.addArgument("-e", "--encryptionMode")
                .choices("CBC", "ECB").setDefault("CBC")
                .help("Encryption mode");
        parser.addArgument("-k", "--keystore")
                .help("Path to keystore");
        parser.addArgument("-n", "--index")
                .help("Key index");

        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch(ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }

        Console console = System.console();
        console.printf("Please enter password to keystore...\n");
        char[] keystorePassword = console.readPassword();
        console.printf("Please enter password to key...\n");
        char[] keyPassword = console.readPassword();

        FileCrypto.convertFile(
                ns.getString("keystore"),
                ns.getString("index"),
                keystorePassword,
                keyPassword,
                ns.getString("scheme"),
                ns.getString("encryptionMode"),
                ns.getString("mode").equals("e") ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE,
                ns.getString("fileIn"),
                ns.getString("fileOut"));
    }
}
