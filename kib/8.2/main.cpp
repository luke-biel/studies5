#include <stdint.h>
#include <iostream>


//  Windows
#ifdef _WIN32

#include <intrin.h>
uint64_t rdtsc(){
    return __rdtsc();
}

//  Linux/GCC
#else

uint64_t rdtsc(){
    unsigned int lo,hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
}

#endif

int main(void) {

	uint64_t b4 = rdtsc();

	for(int i = 0; i < 10; i++) {
		int x = 3;
		x += 3;
		x = x * i;
	}

	uint64_t af = rdtsc();
	std::cout << b4 << " - " << af << " = " << af - b4;
	
	return 0;
}
