<?php
/**
 * Created by PhpStorm.
 * User: dot
 * Date: 03.12.16
 * Time: 00:53
 */

session_start();

$is_logged = isset($_SESSION['user_id']);
$token = md5(uniqid('auth', true));
$_SESSION['token'] = $token;

?>

<html>
<head>
	<?php if($is_logged): ?>
	<meta http-equiv="refresh" content="0; url=summary.php" />
	<?php endif; ?>
	<link rel="stylesheet" href="reset.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<meta name="google-signin-client_id" content="167184710899-fv95t4e6bdbtutbkqnr2d44ua949idla.apps.googleusercontent.com">
	<script src="signin.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<title>Index</title>
</head>
<body>

	<div id="content">
		<form id="login" method="post" action="register_submit.php">
			<input name="name" type="text" placeholder="Username" required>
			<input name="pass" type="password" placeholder="Password" required>
			<input name="token" type="hidden" value="<?php echo $token; ?>">
			<button>Register</button>
			<div class="extra_platforms">
				<div class="g-recaptcha" data-sitekey="6LcWARQUAAAAAJUHM3YNaaL1SXwAaa0gUoQoxb5d"></div>
			</div>
		</form>
		<div class="extra_platforms" >
			<div class="g-signin2" data-onsuccess="onSignIn"></div>
		</div>
	</div>

	<div id="login_info">
		<a href="login.php">Log in</a>
	</div>
</body>
</html>
