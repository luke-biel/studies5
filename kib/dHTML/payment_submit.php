<?php
/**
 * Created by PhpStorm.
 * User: dot
 * Date: 03.12.16
 * Time: 01:50
 */

session_start();

$redirect_to = "login.php";

if(isset( $_SESSION['user_id'] )) {
	$message = 'Users is already logged in';
	$redirect_to = "summary.php";
} if(!isset( $_POST['to'], $_POST['amount'])) {
	$message = 'Please enter a valid values';
} elseif (is_numeric($_POST['to']) != true || is_numeric($_POST['amount']) != true) {
	$message = "Values must be numeric";
} else {
	$dbHostname = 'localhost';
	$dbUsername = 'dot';
	$dbPassword = '1234';
	$dbName = 'dot';

	try {
		$dbh = new PDO("pgsql:host=$dbHostname;dbname=$dbName", $dbUsername, $dbPassword);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt = $dbh->prepare("INSERT INTO payments (user_id, payment_to, amount, title, receiver) values (:user_id, :to, :amt, :title, :receiver);");
		$stmt->bindParam(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);
		$stmt->bindParam(':to', $_POST['to'], PDO::PARAM_STR);
		$stmt->bindParam(':amt', $_POST['amount'], PDO::PARAM_INT);
		$stmt->bindParam(':title', $_POST['title'], PDO::PARAM_STR);
		$stmt->bindParam(':receiver', $_POST['receiver'], PDO::PARAM_STR);


		$stmt->execute();
		$message = 'Your payment was successfull';
		$redirect_to = "summary.php";

	} catch (Exception $e) {
		$message = 'We are unable to process your request. Please try again later. Error: ' . $e->getMessage();
		$redirect_to = "index.php";
	}
}

?>

<html>
<head>
	<meta http-equiv="refresh" content="5; url=<?php echo $redirect_to ?>" />
	<link rel="stylesheet" href="reset.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<title>Index</title>
</head>
<body>

<div id="content">
	<?php echo $message ?>
</div>
</body>
</html>
