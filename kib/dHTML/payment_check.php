<?php
/**
 * Created by PhpStorm.
 * User: dot
 * Date: 03.12.16
 * Time: 12:15
 */

session_start();

if(!isset($_SESSION['user_id'])) {
	$message = 'You must be logged in to view this page!';
	$redirect_to = "login.php";
} else {
	$to = $_POST['to'];
	$amount = $_POST['amount'];
	$receiver = $_POST['receiver'];
	$title = $_POST['title'];
	$message .= "<form id='login' method='post' action='payment_submit.php'>";
	$message .= "<input type='text' name='to' value='$to' readonly>";
	$message .= "<input type='text' name='amount' value='$amount' readonly>";
	$message .= "<input type='text' name='receiver' value='$receiver' readonly>";
	$message .= "<input type='text' name='title' value='$title' readonly>";
	$message .= "<button id='send'>OK</button>";
	$message .= "</form>";
}

?>

<html>
<head>
	<?php if(!isset($_SESSION['user_id'])): ?>
		<meta http-equiv="refresh" content="0; url=login.php" />
	<?php endif; ?>
	<link rel="stylesheet" href="reset.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<title>Index</title>
</head>
<body>

<div id="content">
	<?php echo $message ?>
</div>
<div id="login_info">
	<a href="payment.php">Make payment</a>
</div>
</body>
</html>