<?php
/**
 * Created by PhpStorm.
 * User: dot
 * Date: 03.12.16
 * Time: 01:50
 */

session_start();

$redirect_to = "login.php";

if(!isset( $_SESSION['user_id'] )) {
	$message = 'Users is not logged in';
	$redirect_to = "login.php";
} else {
	$dbHostname = 'localhost';
	$dbUsername = 'dot';
	$dbPassword = '1234';
	$dbName = 'dot';

	try {
		$dbh = new PDO("pgsql:host=$dbHostname;dbname=$dbName", $dbUsername, $dbPassword);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


		$getUser = $dbh->prepare("SELECT user_mode FROM users WHERE user_id = :user_id");
		$getUser->bindParam(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);
		$getUser->execute();

		$is_admin = $getUser->fetch()["user_mode"] == true;
		if(!$is_admin) {
			$redirect_to = "summary.php";
			$message = "User is not an admin";
		} else {

			if ($_POST['action'] == "Accept") {
				$stmt = $dbh->prepare("UPDATE payments SET approved = TRUE where payment_id = :id;");
				$message = 'Payment accepted!';
			} else {
				$stmt = $dbh->prepare("DELETE FROM payments WHERE payment_id = :id;");
				$message = 'Payment rejected!';
			}
			$stmt->bindParam(':id', $_POST['id'], PDO::PARAM_INT);
			$stmt->execute();
			$redirect_to = "accept.php";
		}

	} catch (Exception $e) {
		$message = 'We are unable to process your request. Please try again later. Error: ' . $e->getMessage();
		$redirect_to = "index.php";
	}
}

?>

<html>
<head>
	<meta http-equiv="refresh" content="5; url=<?php echo $redirect_to ?>" />
	<link rel="stylesheet" href="reset.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<title>Index</title>
</head>
<body>

<div id="content">
	<?php echo $message ?>
</div>
</body>
</html>
