<?php
/**
 * Created by PhpStorm.
 * User: dot
 * Date: 03.12.16
 * Time: 01:50
 */

session_start();

$redirect_to = "login.php";

if(isset( $_SESSION['user_id'] )) {
	$message = 'Users is already logged in';
	$redirect_to = "summary.php";
} if(!isset( $_POST['name'], $_POST['pass'])) {
	$message = 'Please enter a valid name and password';
} elseif (strlen( $_POST['name']) > 20 || strlen($_POST['name']) < 4) {
	$message = 'Incorrect Length for Username';
} elseif (strlen( $_POST['pass']) > 40 || strlen($_POST['pass']) < 8) {
	$message = 'Incorrect Length for Password';
} elseif (preg_match("/^[a-zA-Z0-9\\.\\@]*$/", $_POST['name']) != true) {
	$message = "Username must contain only alphanumeric characters or . or @.";
} else {
	$name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
	$pass = filter_var($_POST['pass'], FILTER_SANITIZE_STRING);
	$pass = hash("sha512", $pass);

	$dbHostname = 'localhost';
	$dbUsername = 'dot';
	$dbPassword = '1234';
	$dbName = 'dot';

	try {
		$dbh = new PDO("pgsql:host=$dbHostname;dbname=$dbName", $dbUsername, $dbPassword);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt = $dbh->prepare("SELECT user_id, username, password FROM users WHERE username = :username AND password = :password");
		$stmt->bindParam(':username', $name, PDO::PARAM_STR);
		$stmt->bindParam(':password', $pass, PDO::PARAM_STR, 40);

		$stmt->execute();
		$user_id = $stmt->fetchColumn();

		if ($user_id == false) {
			$message = 'Login Failed';
			$redirect_to = "index.php";
		} else {
			$url = "https://www.google.com/recaptcha/api/siteverify";
			$data = array(
				'secret' => '6LcWARQUAAAAAAelNLDyi3APUsDZqCydSGEnmiG0',
				'response' => $_POST['g-recaptcha-response']);
			$options = array(
				'http' => array(
					'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
					'method'  => 'POST',
					'content' => http_build_query($data)
				)
			);
			$context  = stream_context_create($options);
			$result = file_get_contents($url, false, $context);
			$json = json_decode($result);

			if(!$json->success) {
				$message = 'Failed to login due to reCaptha error!';
				$redirect_to = 'login.php';

			} else {

				$_SESSION['user_id'] = $user_id;
				$message = 'You are now logged in';

				$redirect_to = "summary.php";
			}

		}


	} catch (Exception $e) {
		$message = 'We are unable to process your request. Please try again later"';
		$redirect_to = "index.php";
	}
}

?>

<html>
<head>
	<meta http-equiv="refresh" content="2; url=<?php echo $redirect_to ?>" />
	<link rel="stylesheet" href="reset.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<title>Index</title>
</head>
<body>

<div id="content">
	<?php echo $message ?>
</div>

</body>
</html>