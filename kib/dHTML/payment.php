<?php session_start(); ?>

<html>
<head>
	<?php if(!isset($_SESSION['user_id'])): ?>
		<meta http-equiv="refresh" content="0; url=login.php" />
	<?php endif; ?>
	<link rel="stylesheet" href="reset.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<title>Index</title>
</head>
<body>

<div id="content">
	<form id='login' method="post" action="payment_check.php">
		<input name="to" type="text" placeholder="Account number" required>
		<input name="receiver" type="text" placeholder="Receiver" required>
		<input name="title" type="text" placeholder="Title" required>
		<input name="amount" type="text" placeholder="Amount" required>
		<button>Make payment</button>
	</form>
</div>
<div id="login_info">
	<a href="summary.php">Summary</a>
</div>
</body>
</html>
