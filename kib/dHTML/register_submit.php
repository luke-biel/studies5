<?php
/**
 * Created by PhpStorm.
 * User: dot
 * Date: 03.12.16
 * Time: 01:20
 */

session_start();

$success = false;

if(!isset( $_POST['name'], $_POST['pass'], $_POST['token'])) {
	$message = 'Please enter a valid username and pass';
} elseif( $_POST['token'] != $_SESSION['token']) {
	$message = 'Invalid form submission';
} elseif (strlen( $_POST['name']) > 20 || strlen($_POST['name']) < 4) {
	$message = 'Incorrect Length for username';
} elseif (strlen( $_POST['pass']) > 40 || strlen($_POST['pass']) < 8) {
	$message = 'Incorrect Length for password';
} elseif (preg_match("/^[a-zA-Z0-9\\.\\@]*$/", $_POST['name']) != true) {
	$message = "Username must contain only alphanumeric characters or . or @.";
} else {
$user = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
	$pass = filter_var($_POST['pass'], FILTER_SANITIZE_STRING);

	$pass = hash("sha512", $pass);

	$dbHostname = 'localhost';
	$dbUsername = 'dot';
	$dbPassword = '1234';
	$dbName = 'dot';
	$url = "https://www.google.com/recaptcha/api/siteverify";
	$data = array(
		'secret' => '6LcWARQUAAAAAAelNLDyi3APUsDZqCydSGEnmiG0',
		'response' => $_POST['g-recaptcha-response']);
	$options = array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($data)
		)
	);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
	$json = json_decode($result);

	if(!$json->success) {
		$message = 'Failed to login due to reCaptha error!';
		$redirect_to = false;
	} else {
		try {

			$dbh = new PDO("pgsql:host=$dbHostname;dbname=$dbName", $dbUsername, $dbPassword);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$stmt = $dbh->prepare("INSERT INTO users (username, password) VALUES (:username, :password )");
			$stmt->bindParam(':username', $user, PDO::PARAM_STR);
			$stmt->bindParam(':password', $pass, PDO::PARAM_STR, 40);
			$stmt->execute();
			unset($_SESSION['token']);
			$message = "New user added!";
			$success = true;
		} catch (Throwable $e) {
			if ($e->getCode() == 23000) {
				$message = "Username already exists!";
			} else {
				$message = "We were unable to process request!";
			}
		}
	}
}

?>

<html>
<head>
	<?php if(!$success): ?>
		<meta http-equiv="refresh" content="5; url=index.php" />
	<?php else: ?>
		<meta http-equiv="refresh" content="5; url=login.php" />
	<?php endif; ?>
	<link rel="stylesheet" href="reset.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<title>Index</title>
</head>
<body>

	<div id="content">
		<?php echo $message ?>
	</div>
</body>
</html>