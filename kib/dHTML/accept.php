<?php
/**
 * Created by PhpStorm.
 * User: dot
 * Date: 03.12.16
 * Time: 12:15
 */

session_start();

if(!isset($_SESSION['user_id'])) {
	$message = 'You must be logged in to view this page!';
	$redirect_to = "login.php";
} else {
	$dbHostname = 'localhost';
	$dbUsername = 'dot';
	$dbPassword = '1234';
	$dbName = 'dot';
	try {
		$dbh = new PDO("pgsql:host=$dbHostname;dbname=$dbName", $dbUsername, $dbPassword);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$getUser = $dbh->prepare("SELECT user_mode FROM users WHERE user_id = :user_id");
		$getUser->bindParam(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);
		$getUser->execute();

		$is_admin = $getUser->fetch()["user_mode"] == true;
		if(!$is_admin) {
			$redirect_to = "summary.php";
		}

		$stmt = $dbh->prepare("SELECT payments.payment_id, payments.payment_to, payments.amount, payments.title, payments.receiver, users.username FROM payments  INNER JOIN users on payments.user_id = users.user_id WHERE payments.approved = FALSE");
		$stmt->execute();


		foreach ($stmt->fetchAll() as $item) {
			$message .= "<form action='accept_submit.php' method='POST'>";
			$to = $item['payment_to'];
			$amt = $item['amount'];
			$receiver = $item['receiver'];
			$title = $item['title'];
			$id = $item['payment_id'];
			$name = $item['username'];
			$message .= "<input type='text' name='name' value='$name' readonly />";
			$message .= "<input type='hidden' name='id' value='$id' readonly />";
			$message .= "<input type='text' name='to' value='$to' readonly />";
			$message .= "<input type='text' name='receiver' value='$receiver' readonly />";
			$message .= "<input type='text' name='title' value='$title'readonly />";
			$message .= "<input type='text' name='amt' value='$amt' readonly />";
			$message .= "<button class='accept_admin' name='action' value='Accept'>Accept</button>";
			$message .= "<button class='accept_admin' name='action' value='Delete'>Reject</button>";
			$message .= "</form><br />";
		}

	} catch(Throwable $e) {
		$message = 'We were unable to process request!';
	}
}

?>

<html>
<head>
	<?php if(!isset($_SESSION['user_id'])): ?>
		<meta http-equiv="refresh" content="0; url=<?php echo $redirect_to ?>" />
	<?php endif; ?>
	<link rel="stylesheet" href="reset.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<title>Index</title>
</head>
<body>

<?php if(!isset($redirect_to)): ?>
	<div id="content">
		<?php echo $message ?>
	</div>
	<div id="login_info">
		<a href="summary.php">Back</a>
	</div>
<?php endif; ?>
</body>
</html>