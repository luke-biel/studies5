<?php
/**
 * Created by PhpStorm.
 * User: dot
 * Date: 03.12.16
 * Time: 12:15
 */

session_start();

if(!isset($_SESSION['user_id'])) {
	$message = 'You must be logged in to view this page!';
	$redirect_to = "login.php";
} else {
	$dbHostname = 'localhost';
	$dbUsername = 'dot';
	$dbPassword = '1234';
	$dbName = 'dot';
	try {
		$dbh = new PDO("pgsql:host=$dbHostname;dbname=$dbName", $dbUsername, $dbPassword);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$getUser = $dbh->prepare("SELECT user_mode FROM users WHERE user_id = :user_id");
		$getUser->bindParam(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);
		$getUser->execute();

		$is_admin = $getUser->fetch()["user_mode"] == true;

		$stmt = $dbh->prepare("SELECT payment_id, payment_to, amount, title, receiver, approved FROM payments WHERE payments.user_id = :user_id AND payments.approved = TRUE");
		$stmt->bindParam(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);
		$stmt->execute();


		$message .= "<ul>";
		$message .= "<li>to -> title -> amount</li>";
		foreach ($stmt->fetchAll() as $item) {
			$to = $item['payment_to'];
			$amt = $item['amount'];
			$receiver = $item['receiver'];
			$title = $item['title'];
			$message .= "<li>$receiver($to) -> $title -> $amt</li>";
		}
		$message .= "</ul>";
		
	} catch(Throwable $e) {
		$message = 'We were unable to process request!';
	}
}

?>

<html>
<head>
	<?php if(!isset($_SESSION['user_id'])): ?>
		<meta http-equiv="refresh" content="0; url=login.php" />
	<?php endif; ?>
	<link rel="stylesheet" href="reset.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<title>Index</title>
</head>
<body>

	<div id="content">
		<?php echo $message ?>
	</div>
	<div id="login_info">
		<a href="payment.php">Make payment</a>
		<?php if($is_admin): ?>
		<a href="accept.php">Accept pending transactions</a>
		<?php endif; ?>
	</div>
</body>
</html>