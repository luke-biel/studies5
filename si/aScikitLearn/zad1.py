from sklearn import datasets
import matplotlib.pyplot as plt
import pandas

iris = datasets.load_iris()

for i in range(0, 4):
    items = [item[i] for item in iris.data]
    df = pandas.DataFrame(items)
    df.columns = [iris.feature_names[i]]
    print(df.describe())
    df.plot(kind='box')
    df.plot(kind='hist')
    print("Len: ", len(set(items)))

plt.show()
