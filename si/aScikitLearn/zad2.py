from sklearn.model_selection import train_test_split
from sklearn import datasets
from sklearn import linear_model
from sklearn import tree
from sklearn import neighbors

iris = datasets.load_iris()

X_train, X_test, y_train, y_test = train_test_split(iris.data, iris.target, test_size=0.2)

clf_lm = linear_model.LogisticRegression().fit(X_train, y_train)
print("Linear model: ", clf_lm.score(X_test, y_test))


clf_t = tree.DecisionTreeClassifier().fit(X_train, y_train)
print("Decision tree: ", clf_t.score(X_test, y_test))


clf_n = neighbors.KNeighborsClassifier().fit(X_train, y_train)
print("Nearest neightbors: ", clf_n.score(X_test, y_test))
