
function iterate(p0, r, n::Int)
    p = p0
    for i = 1:n
        p = p + r * p * (1 - p)
    end
    return p
end

function iterateCut(p0, r, n::Int)
    p = p0
    for i = 1:n
        p = round(p + r * p * (1 - p), 3)
    end
    return p
end
