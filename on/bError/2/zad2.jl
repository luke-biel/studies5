function matcond(n::Int, c::Float64)
# Function generates a random square matrix A of size n with
# a given condition number c.
# Inputs:
#	n: size of matrix A, n>1
#	c: condition of matrix A, c>= 1.0
#
# Usage: matcond (10, 100.0);
#
# Pawel Zielinski
        if n < 2
         error("size n should be > 1")
        end
        if c< 1.0
         error("condition number  c of a matrix  should be >= 1.0")
        end
        (U,S,V)=svd(rand(n,n))
        return U*diagm(linspace(1.0,c,n))*V'
end

function hilb(n::Int)
# Function generates the Hilbert matrix  A of size n,
#  A (i, j) = 1 / (i + j - 1)
# Inputs:
#	n: size of matrix A, n>0
#
#
# Usage: hilb (10)
#
# Pawel Zielinski
        if n < 1
         error("size n should be > 0")
        end
        A= Array(Float64, n,n)
        for j=1:n, i=1:n
                A[i,j]= 1 / (i + j - 1)
        end
        return A
end

function findSolutionGauss(M::Array{Float64, 2})
    n = size(M)[1]
    b = M * ones(n)
    for y = 1:n
        for yy = y+1:n
            div = M[yy, y] / M[y, y]
            for x = y:n
                M[yy, x] = M[yy, x] - M[y, x] * div
            end
            b[yy] = b[yy] - b[y] * div
        end
    end

    println(M)
    
    for y = n:-1:1
        i = M[y, y]
        b[y] = b[y] / M[y, y]
        for yy = y-1:-1:1
            div = M[yy, y] / M[y, y]
            M[yy, y] = 0
            b[yy] = b[yy] - b[y] * div
        end
    end    
    
    return b
end

function findSolutionInv(M::Array{Float64, 2})
    b = M * ones(size(M)[1])
    return inv(M) * b
end

function genRand()
    for i = 5:5:15
       for c = [1.0, 10.0, 10.0^3, 10.0^7, 10.0^12, 10.0^16]
           R = matcond(i, c)
           Ri = copy(R)
           println("I = ", i, ", c = ", c)
           println("Gau:\t", findSolutionGauss(R))
           println("Inv:\t", findSolutionInv(Ri))
       end
    end
end
