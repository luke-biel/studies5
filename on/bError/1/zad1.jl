# Łukasz Biel

# vektory i ich długość
x = [2.718281828, -3.141592654, 1.414213562, 0.577215664, 0.301029995]
y = [1486.2497, 878366.9879, -22.37492, 4773714.647, 0.000185049]
n = 5

# pierwsza metoda liczenia i. skalarnego (vektor1, vektor2, dł. wektora)
function scalar1(a, b, n)
    S = 0.0
    for i=1:n
        S = S + (a[i] * b[i])
    end
    return S
end

# druga metoda liczenia i. skalarnego (vektor1, vektor2, dł. wektora)
function scalar2(a, b, n)
    S = 0.0
    for i=1:n
        S = S + (a[n-i+1] * b[n-i+1])
    end
    return S
end

# trzecia metoda liczenia iloczynu skalarnego (vektor1, vektor2, dł. wektora)
function scalar3(a, b, n)
    sm = []
    bg = []
    for i=1:n
        x = a[i] * b[i]
        if x > 0
            push!(bg, x)
        else
            push!(sm, x)
        end
    end
    sm = sort(sm)
    bg = sort(bg, rev=true)
    Ssm = 0.0
    Sbg = 0.0
    for i=1:length(sm)
        Ssm = Ssm + sm[i]
    end
    for i=1:length(bg)
        Sbg = Sbg + bg[i]
    end
    return Ssm + Sbg
end

# czwarta metoda liczenia iloczynu skalarnego (vektor1, vektor2, dł. wektora)
function scalar4(a, b, n)
    sm = []
    bg = []
    for i=1:n
        x = a[i] * b[i]
        if x > 0
            push!(bg, x)
        else
            push!(sm, x)
        end
    end
    sm = sort(sm, rev=true)
    bg = sort(bg)
    Ssm = 0.0
    Sbg = 0.0
    for i=1:length(sm)
        Ssm = Ssm + sm[i]
    end
    for i=1:length(bg)
        Sbg = Sbg + bg[i]
    end
    return Ssm + Sbg
end

println("Float32\tFloat64")
x64 = convert(Array{Float64, 1}, x)
y64 = convert(Array{Float64, 1}, y)
x32 = convert(Array{Float32, 1}, x)
y32 = convert(Array{Float32, 1}, y)

println(scalar1(x32, y32, n), "\t", scalar1(x64, y64, n))
println(scalar2(x32, y32, n), "\t", scalar2(x64, y64, n))
println(scalar3(x32, y32, n), "\t", scalar3(x64, y64, n))
println(scalar4(x32, y32, n), "\t", scalar4(x64, y64, n))

