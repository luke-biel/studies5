e1 = (-2, 1)
e2 = (-2, 2)
e3 = (-2, 1.99999999999999)
e4 = (-1, 1)
e5 = (-1, -1)
e6 = (-1, 0.75)
e7 = (-1, 0.25)

using PyPlot

function iterate(e, n)
    c, x0 = e
    c = convert(Float64, c)
    x0 = convert(Float64, x0)
    x = x0
    out = Array{Float64}(n+1)
    out[1] = x0
    for i = 1:n
        x = x^2 + c
        out[i+1] = x
    end
    return out
end

rnge = collect(1:41)


