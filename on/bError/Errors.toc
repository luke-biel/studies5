\contentsline {section}{\numberline {1}Uwarunkowanie programu obliczaj\IeC {\k a}cego iloczyn skalarny (zadanie 1)}{2}
\contentsline {subsection}{\numberline {1.1}Opis}{2}
\contentsline {subsection}{\numberline {1.2}Wyniki}{2}
\contentsline {subsubsection}{\numberline {1.2.1}Arytmetyka Float32}{2}
\contentsline {subsubsection}{\numberline {1.2.2}Arytmetyka Float64}{2}
\contentsline {subsection}{\numberline {1.3}Wnioski}{2}
\contentsline {section}{\numberline {2}Rozwi\IeC {\k a}zywanie uk\IeC {\l }ad\IeC {\'o}w r\IeC {\'o}wna\IeC {\'n} w postaci macierzy (zadanie 2)}{3}
\contentsline {subsection}{\numberline {2.1}Opis}{3}
\contentsline {subsection}{\numberline {2.2}Implementacja}{3}
\contentsline {subsection}{\numberline {2.3}Wyniki}{3}
\contentsline {subsubsection}{\numberline {2.3.1}Macierze randomowe}{3}
\contentsline {subsubsection}{\numberline {2.3.2}Macierze Hilberta}{3}
\contentsline {subsection}{\numberline {2.4}Wnioski}{4}
\contentsline {section}{\numberline {3}Wielomian Wilkinsona (zadanie 3)}{4}
\contentsline {subsection}{\numberline {3.1}Opis}{4}
\contentsline {subsection}{\numberline {3.2}Aplikacja oraz wyniki}{4}
\contentsline {subsection}{\numberline {3.3}Wnioski}{4}
\contentsline {section}{\numberline {4}Uwarunkowanie r\IeC {\'o}wnania rekurencyjnego (zadanie 4)}{4}
\contentsline {subsection}{\numberline {4.1}Opis}{4}
\contentsline {subsection}{\numberline {4.2}Wyniki}{6}
\contentsline {subsection}{\numberline {4.3}Wnioski}{6}
\contentsline {section}{\numberline {5}Ci\IeC {\k a}gi kwadrat\IeC {\'o}w (zadanie 5)}{6}
\contentsline {subsection}{\numberline {5.1}Opis}{6}
\contentsline {subsection}{\numberline {5.2}Wyniki}{6}
\contentsline {subsection}{\numberline {5.3}Wnioski}{6}
