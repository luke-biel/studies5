module Interpolation
export differenceQuotient
export newtonValueAt
export drawNnfx

using PyPlot

function q(n::Int, xi::Float64, x::Vector{Float64})
    qn::Float64 = 1
    for i in 1:(n-1)
        qn *= xi - x[i]
    end
    return qn
end

function differenceQuotient(x::Vector{Float64}, f::Vector{Float64})
    n = length(x)
    assert(length(f) == n)
    fx::Array{Float64} = Array{Float64}(n)
    for i in 1:n
        sum::Float64 = 0
        for k in 1:i-1
            sum += fx[k] * q(k, x[i], x)
        end
        mul::Float64 = 1
        for k in 1:i-1
            mul *= x[i] - x[k]
        end
        fx[i] = (f[i] - sum) / mul
    end
    return fx
end


function w(n::Int, k::Int, x::Vector{Float64}, fx::Vector{Float64}, t::Float64)
    if (k == n)
        return fx[n]
    end
    return fx[k] + (t - x[k]) * w(n, k+1, x, fx, t)
end

function newtonValueAt(x::Vector{Float64}, fx::Vector{Float64}, t::Float64)
    n = length(x)
    assert(length(fx) == n)
    nt::Float64 = 0
    return w(n, 1, x, fx, t)
end

function drawNnfx(f::Function, a::Float64, b::Float64, n::Int, name::ASCIIString = "fig.png", yaxis::Array{Float64} = [])
    h = (b-a)/n
    x = Array{Float64}(n+1)
    y = Array{Float64}(n+1)
    for i in 0:n
        x[i+1] = a + i*h
        y[i+1] = f(x[i+1])
    end
    nx = Array{Float64}(101)
    ny = Array{Float64}(101)
    nh = (b-a) / 100
    for i in 0:100
        nx[i+1] = a + i*nh
        ny[i+1] = newtonValueAt(x, differenceQuotient(x, y), nx[i+1])
    end
    clf()
    plot(nx, ny)
    if (length(yaxis) == 2)
        gca()[:set_ylim](yaxis)        
    end
    savefig(name)
    clf()
end

function drawNnRand(f::Function, a::Float64, b::Float64, n::Int, name::ASCIIString = "fig.png", yaxis::Array{Any} = [])
    x = Array{Float64}(n+1)
    y = Array{Float64}(n+1)
    d = (b - a)
    rand!(x, [0.01*x + a for x in 0:d*100])
    for i in 1:n+1
        y[i] = f(x[i])
    end
    nx = Array{Float64}(101)
    ny = Array{Float64}(101)
    nh = (b-a) / 100
    for i in 0:100
        nx[i+1] = a + i*nh
        ny[i+1] = newtonValueAt(x, differenceQuotient(x, y), nx[i+1])
    end
    clf()
    plot(nx, ny)
    if (length(yaxis) == 2)
        gca()[:set_ylim](yaxis)        
    end
    savefig(name)
    clf()
end


end
