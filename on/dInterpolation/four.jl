include("Interpolation.jl")

using PyPlot

x4a = linspace(0, 1, 100)
y4a = exp(x4a)

ylim = [minimum(y4a), maximum(y4a)]
gca()[:set_ylim](ylim)

plot(x4a, y4a)
savefig("fig_4a.png")
clf()

Interpolation.drawNnfx(x -> e^x, 0.0, 1.0, 2, "fig_4a2.png", ylim)

Interpolation.drawNnfx(x -> e^x, 0.0, 1.0, 5, "fig_4a5.png", ylim)

Interpolation.drawNnfx(x -> e^x, 0.0, 1.0, 10, "fig_4a10.png", ylim)

Interpolation.drawNnfx(x -> e^x, 0.0, 1.0, 15, "fig_4a15.png", ylim)

x4b = linspace(-1, 1, 100)
y4b = [(x^2) * sin(x) for x in x4b]

ylim = [minimum(y4b), maximum(y4b)]
gca()[:set_ylim](ylim)

plot(x4b, y4b)
savefig("fig_4b.png")
clf()

Interpolation.drawNnfx(x -> (x^2) * sin(x), -1.0, 1.0, 3, "fig_4b3.png", ylim)

Interpolation.drawNnfx(x -> (x^2) * sin(x), -1.0, 1.0, 5, "fig_4b5.png", ylim)

Interpolation.drawNnfx(x -> (x^2) * sin(x), -1.0, 1.0, 10, "fig_4b10.png", ylim)

Interpolation.drawNnfx(x -> (x^2) * sin(x), -1.0, 1.0, 15, "fig_4b15.png", ylim)
