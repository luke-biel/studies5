include("Interpolation.jl")

using PyPlot

xta = linspace(0, 1, 100)
yta = exp(xta*2)

ylim = [minimum(yta), maximum(yta)]
gca()[:set_ylim](ylim)

plot(xta, yta)
savefig("fig_ta.png")
clf()

Interpolation.drawNnfx(x -> e^(x*2), 0.0, 1.0, 5, "fig_ta5.png", ylim)

Interpolation.drawNnfx(x -> e^(x*2), 0.0, 1.0, 15, "fig_ta15.png", ylim)

ytb = sin(xta*2) + exp(cos(xta))

ylim = [minimum(ytb), maximum(ytb)]
gca()[:set_ylim](ylim)

plot(xta, ytb)
savefig("fig_tb.png")
clf()

Interpolation.drawNnfx(x -> sin(x*2) + exp(cos(x)), 0.0, 1.0, 5, "fig_tb5.png", ylim)

Interpolation.drawNnfx(x -> sin(x*2) + exp(cos(x)), 0.0, 1.0, 15, "fig_tb15.png", ylim)
