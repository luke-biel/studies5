include("Interpolation.jl")

using PyPlot

x5a = linspace(-1, 1, 100)
y5a = abs(x5a)

ylim = [minimum(y5a), maximum(y5a)]
gca()[:set_ylim](ylim)

plot(x5a, y5a)
savefig("fig_5a.png")
clf()

Interpolation.drawNnfx(abs, -1.0, 1.0, 5, "fig_5a5.png", ylim)

Interpolation.drawNnfx(abs, -1.0, 1.0, 10, "fig_5a10.png", ylim)

Interpolation.drawNnfx(abs, -1.0, 1.0, 15, "fig_5a15.png", ylim)

x5b = linspace(-5, 5, 100)
y5b = [1 / (1 + x ^ 2) for x in x5b]

ylim = [minimum(y5b), maximum(y5b)]
gca()[:set_ylim](ylim)

plot(x5b, y5b)
savefig("fig_5b.png")
clf()

Interpolation.drawNnfx(x -> 1 / (1 + x^2), -5.0, 5.0, 5, "fig_5b5.png", ylim)

Interpolation.drawNnfx(x -> 1 / (1 + x^2), -5.0, 5.0, 10, "fig_5b10.png", ylim)

Interpolation.drawNnfx(x -> 1 / (1 + x^2), -5.0, 5.0, 15, "fig_5b15.png", ylim)

Interpolation.drawNnfx(x -> 1 / (1 + x^2), -5.0, 5.0, 50, "fig_tc50.png", ylim)
