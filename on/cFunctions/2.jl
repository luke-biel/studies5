function newton(f::Function, pf::Function, x0::Float64, delta::Float64, epsilon::Float64, maxit::Int)
    v = f(x0)
    if abs(v) < epsilon
        return (x0, v, 0, 2)
    end
    for k in 1:maxit
        x1 = x0 - (v / pf(x0))
        v = f(x1)
        if abs(x1 - x0) < delta || abs(v) < epsilon
            return (x1, f(x1), k, 0)
        end
        x0 = x1
    end
    return (x0, f(x0), maxit, 1)
end
