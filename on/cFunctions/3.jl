macro swap(x,y)
   quote
      local tmp = $(esc(x))
      $(esc(x)) = $(esc(y))
      $(esc(y)) = tmp
    end
end

function euler(f::Function, a::Float64, b::Float64, delta::Float64, epsilon::Float64, maxit::Int)
    u = f(a)
    v = f(b)
    for k in 1:maxit
        if abs(u) > abs(v)
            @swap(a, b)
            @swap(u, v)
        end
        s = (b - a) / (v - u)
        b = a
        v = u
        a = a - (u * s)
        u = f(a)
        println("it")
        if abs(b - a) < delta || u < epsilon
            return (a, u, k, 0)
        end
    end
    return (a, u, maxit, 1)
end
