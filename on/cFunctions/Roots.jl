module Roots

macro swap(x,y)
    quote
        local tmp = $(esc(x))
        $(esc(x)) = $(esc(y))
        $(esc(y)) = tmp
    end
end

function bisect(f::Function, a::Float64, b::Float64, delta::Float64, epsilon::Float64)
    if f(a) * f(b) >= 0 # no mocy twierdzenia Darboux
        return (0.0, 0.0, 0.0, 1)
    end
    u = f(a)
    v = f(b)
    e = b - a
    i = 0
    while true
        e = e/2
        c = a + e
        w = f(c)
        if abs(e) < delta || abs(w) < epsilon
            return (c, f(c), i, 0)
        end
        if sign(w) != sign(u)
            b = c
            v = w
        else
            a = c
            u = w
        end
        i = i + 1
    end
end

function newton(f::Function, pf::Function, x0::Float64, delta::Float64, epsilon::Float64, maxit::Int)
    v = f(x0)
    if abs(v) < epsilon
        return (x0, v, 0, 2)
    end
    for k in 1:maxit
        x1 = x0 - (v / pf(x0))
        v = f(x1)
        if abs(x1 - x0) < delta || abs(v) < epsilon
            return (x1, f(x1), k, 0)
        end
        x0 = x1
    end
    return (x0, f(x0), maxit, 1)
end

function euler(f::Function, a::Float64, b::Float64, delta::Float64, epsilon::Float64, maxit::Int)
    u = f(a)
    v = f(b)
    for k in 1:maxit
        if abs(u) > abs(v)
            @swap(a, b)
            @swap(u, v)
        end
        s = (b - a) / (v - u)
        b = a
        v = u
        a = a - (u * s)
        u = f(a)

        if abs(b - a) < delta || abs(u) < epsilon
            return (a, u, k, 0)
        end
    end
    return (a, u, maxit, 1)
end

end
