include("Roots.jl")

delta = 0.000005
epsilon = 0.000005
delta2 = 0.0001
epsilon2 = 0.0001

test1 = Dict(
    "name" => "Test 1",
    "f"    => x -> (x - 3) ^ 2 - 1,
    "pf"   => x -> 2 * (x - 3),
    "r"    => [2.0, 4.0],
    "a"    => [1.1, 3.2],
    "b"    => [3.0, 5.0],
    "x0"   => [1.75, 3.66],
    "x0x1" => [(1.9, 3.1), (2.1, 7.34)],
    "d"    => delta
)

test2 = Dict(
    "name" => "Test 2",
    "f"    => x -> sin(x) * x^2,
    "pf"   => x -> x * (x * cos(x) + 2 * sin(x)),
    "r"    => [-pi, 0, pi], # and many more
    "a"    => [-4.0, -1.0, 3.0],
    "b"    => [-3.0, 1.0, 4.0],
    "x0"   => [-3.1, -0.27, 3.9],
    "x0x1" => [(-3.1, -2.9), (-0.3, 0.01), (3.0, 3.2)],
    "d"    => delta
)

test3 = Dict(
    "name" => "Test 3",
    "f"    => x -> (x^3) - cos((2 * x)),
    "pf"   => x -> (3 * x ^ 2) + (2 * sin(2 * x)),
    "r"    => [0.647765], # not exacly
    "a"    => [0.0],
    "b"    => [7.2],
    "x0"   => [0.0],
    "x0x1" => [(0.5, 0.7)],
    "d"    => delta
)

test4 = Dict(
    "name" => "Test 4",
    "f"    => x -> 2^x - 1,
    "pf"   => x -> log(2) * 2^x,
    "r"    => [0],
    "a"    => [-0.3],
    "b"    => [0.5],
    "x0"   => [0.2],
    "x0x1" => [(-0.1, 3.0)],
    "d"    => delta
)


test5 = Dict(
    "name" => "do zadania 4",
    "f"    => x -> sin(x) - (x/2) ^ 2,
    "pf"   => x -> cos(x) - x/2,
    "r"    => [1.93375],
    "a"    => [1.5],
    "b"    => [2.0],
    "x0"   => [1.5],
    "x0x1" => [(1.0, 2.0)],
    "d"    => delta
)

test6 = Dict(
    "name" => "do zadania 6, część 1",
    "f"    => x -> float(e) ^ (float(1.0 - x)) - 1,
    "pf"   => x -> -e ^ (1.0 - float(x)),
    "r"    => [1.0],
    "a"    => [0.75],
    "b"    => [1.2],
    "x0"   => [0.75],
    "x0x1" => [(0.7, 1.3)],
    "d"    => delta
)

test7 = Dict(
    "name" => "do zadania 6, część 2",
    "f"    => x -> x * e ^ (Float64(-x)),
    "pf"   => x -> (e ^ (Float64(-x))) * (1 - x),
    "r"    => [0.0],
    "a"    => [-0.2],
    "b"    => [0.15],
    "x0"   => [0.1],
    "x0x1" => [(-0.1, 0.3)],
    "d"    => delta
)

function runtest(test::Dict)
    println("test ", test["name"])
    println("bisekcja:\t", Roots.bisect(test["f"], test["a"][1], test["b"][1], test["d"], test["d"]))
    println("newton:\t", Roots.newton(test["f"], test["pf"], test["x0"][1], test["d"], test["d"], 50))
    println("euler:\t", Roots.euler(test["f"], test["x0x1"][1][1], test["x0x1"][1][2], test["d"], test["d"], 50))
end

#runtest(test5)

# 0 = 3*x - e^x
function zad5()
    println("zad 5")
    println(Roots.bisect(x -> 3*x - e^x, 1.0, 2.0, delta2, epsilon2))
end
#zad5()
runtest(test6)
runtest(test7)
