function bisect(f::Function, a::Float64, b::Float64, delta::Float64, epsilon::Float64)
    if f(a) * f(b) >= 0 # no mocy twierdzenia Darboux
        return (0.0, 0.0, 0.0, 1)
    end
    u = f(a)
    v = f(b)
    e = b - a
    i = 0
    while true
        e = e/2
        c = a + e
        w = f(c)
        if abs(e) < delta || abs(w) < epsilon
            return (c, f(c), i, 0)
        end
        if sign(w) != sign(u)
            b = c
            v = w
        else
            a = c
            u = w
        end
        i = i + 1
    end
end
