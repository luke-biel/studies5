# Łukasz Biel

# pochodna w punkcie x
function derivative(x)
    return cos(x) - 3*sin(3*x)
end

# f(x)
function f(x)
    return sin(x) + cos(3*x)
end

# Wartość pochodnej w punkcie x dla dokładności h
function f_(x, h)
    return (f(x + h) - f(x)) / h
end
    
point = 1

println("f'(x) = ", derivative(point))
for i=0:54
    println("h = 2^$i:\t", f_(point, 2.0^-i))
end
