# Łukasz Biel

# funkcja f(x)
function f(x)
    return sqrt(x^2 + 1) - 1 
end

# funkcja g(x)
function g(x)
    return (x^2)/(sqrt(x^2 + 1) + 1)
end

for i=1:16
    println("$i\t", f(8.0^-i), "\t", g(8.0^-i))
end
