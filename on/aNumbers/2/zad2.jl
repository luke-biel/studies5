# Łukasz Biel

# wylicz epsilon dla Float16
function meps16()
    return Float16(3)*(Float16(4)/Float16(3) - Float16(1)) - Float16(1)
end

# wylicz epsilon dla Float32
function meps32()
    return Float32(3)*(Float32(4)/Float32(3) - Float32(1)) - Float32(1)
end

# wylicz epsilon dla Float64
function meps64()
    return Float64(3)*(Float64(4)/Float64(3) - Float64(1)) - Float64(1)
end

println("cat.\t\tFloat16\t\tFloat32\t\tFloat64")
println("eps:")
println("(builtIn): ", "\t", eps(Float16), "\t", eps(Float32), "\t", eps(Float64))
println("(written): ", "\t", abs(meps16()), "\t", abs(meps32()), "\t", abs(meps64()))
