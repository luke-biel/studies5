#Łukasz Biel

include("macheps.jl")

# wylicz realmax dla Flaot16
function max16()
    c = Float16(2)
    while !isinf((Float16(2) - meps16()) * c*Float16(2))
        c = c*Float16(2)
    end
    return (Float16(2) - meps16()) * c
end

# wylicz realmax dla Flaot32
function max32()
    c = Float32(2)
    while !isinf((Float32(2) - meps32()) * c*Float32(2))
        c = c*Float32(2)
    end
    return (Float32(2) - meps32()) * c
end

# wylicz realmax dla Flaot64
function max64()
    c = Float64(2)
    while !isinf((Float64(2) - meps64()) * c*Float64(2))
        c = c*Float64(2)
    end
    return (Float64(2) - meps64()) * c
end
