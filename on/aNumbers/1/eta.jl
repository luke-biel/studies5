# Łukasz Biel

# wylicz min dla Float16
function eta16()
    eta = Float16(1)
    while Float16(0) + (eta / Float16(2)) > 0
        eta = eta / Float16(2)
    end
    return eta
end

# wylicz eta dla Float32
function eta32()
    eta = Float32(1)
    while Float32(0) + (eta / Float32(2)) > 0
        eta = eta / Float32(2)
    end
    return eta
end

# wylicz eta dla Float64
function eta64()
    eta = Float64(1)
    while Float64(0) + (eta / Float64(2)) > 0
        eta = eta / Float64(2)
    end
    return eta
end

