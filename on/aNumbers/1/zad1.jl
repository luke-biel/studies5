# Łukasz Biel

include("macheps.jl")
include("eta.jl")
include("max.jl")

println("cat.\t\tFloat16\t\tFloat32\t\tFloat64")
println("eps:")
println("(builtIn): ", "\t", eps(Float16), "\t", eps(Float32), "\t", eps(Float64))
println("(written): ", "\t", meps16(), "\t", meps32(), "\t", meps64())
println("eta:")
println("(builtIn): ", "\t", nextfloat(Float16(0)), "\t\t", nextfloat(Float32(0)), "\t\t", nextfloat(Float64(0)))
println("(written): ", "\t", eta16(), "\t\t", eta32(), "\t\t", eta64())
println("max:")
println("(builtin): ", "\t", realmax(Float16), "\t\t", realmax(Float32), "\t", realmax(Float64))
println("(written): ", "\t", max16(), "\t\t", max32(), "\t", max64())
