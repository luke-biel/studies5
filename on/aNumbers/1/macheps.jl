# Łukasz Biel

# wylicz epsilon dla Float16
function meps16()
    me = Float16(1)
    while Float16(1) + (me / Float16(2)) > 1
        me = me / Float16(2)
    end
    return me
end

# wylicz epsilon dla Float32
function meps32()
    me = Float32(1)
    while Float32(1) + (me / Float32(2)) > 1
        me = me / Float32(2)
    end
    return me
end

# wylicz epsilon dla Float64
function meps64()
    me = Float64(1)
    while Float64(1) + (me / Float64(2)) > 1
        me = me / Float64(2)
    end
    return me
end
