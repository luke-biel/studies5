# Łukasz Biel

# najmniejsza liczba spełniająca równanie w zakresie (min, max)
function smallest(min, max)
    # iterator
    c = nextfloat(Float64(min))
    while c * (1.0/c) == 1.0 && c < max
        c = nextfloat(c)
    end
    # nie zwraca niczego, jeśli nie znajdzie takiej liczby
    if c != max
        return c
    end
end

x = smallest(1.0, 2.0)
println("smallest 1 < x < 2 is \t\t", x)
x = smallest(0.0, 1.0)
println("smallest 0 < x is \t\t", x) 
