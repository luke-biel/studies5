module blocksys

include("matrixgen.jl")

export loadMatrix, loadVector, smToVectorB, writeSolutionToFile

function loadMatrix(file::ASCIIString)
    n = 0
    l = 0
    open(file) do f
        n, l = map(x -> parse(Int, x), split(readline(f)))
        dict = spzeros(n, n)
        while !eof(f)
            line = split(readline(f))
            i = parse(Int, line[1])
            j = parse(Int, line[2])
            v = parse(Float64, line[3])
            dict[i, j] = v
        end
        return dict, n, l
    end    
end

function loadVector(file::ASCIIString)
    dict = Dict{Int, Float64}()
    n = 0
    open(file) do f
        n = parse(Int, readline(f))
        i = 1
        while !eof(f)
            dict[i] = parse(Float64, readline(f))
            i += 1
        end
    end
    return dict, n
end

function gaussNoPivot(matrix::SparseMatrixCSC{Float64, Int64}, vector::Dict{Int, Float64}, l::Int, n::Int)
    # Function finds solution to set of equations using gaussian method
    #
    # Input:
    # 	matrix: Matrix stored as dictionary conaining only non-zero elements
    # 	vector: A vector of values to multiply matrix by
    # 	l: size of block
    #	n: size of matrix
    #
    # Usage:
    # 	gaussNoPivot(M, V, 3, 9)
    #
    M = copy(matrix)
    V = copy(vector)

    answer = Array{Float64, 1}(n)
    for y = 1:n
        for yp = y+1:y+2*l
            if yp > n
                break
            end
            if M[yp, y] != 0
                div = M[yp, y] / M[y, y]
                for x = y:y+2*l+1
                    if x > n
                        break
                    end
                    M[yp, x] = M[yp, x] - M[y, x] * div
                end
                V[yp] = V[yp] - V[y] * div
            end            
        end
    end

    for y = n:-1:1
        i = M[y, y]
        answer[y] = V[y] / M[y, y]
        for yy = y-1:-1:y-l-1
            if yy < 1
                break
            end
            if M[yy, y] != 0
                div = M[yy, y] / M[y, y]
                M[yy, y] = 0
                V[yy] = V[yy] - V[y] * div
            end
        end
    end    
    
    return answer
end

function gaussPivoted(matrix::SparseMatrixCSC{Float64, Int64}, vector::Dict{Int, Float64}, l::Int, n::Int)
    M = copy(matrix)
    V = copy(vector)

#    println(full(M))
    
    for y = 1:n
        maxVal = M[y, y]
        maxIdx = y
        
        for yp = y+1:y+2*l+1
            if yp > n
                break
            end
            if M[yp, y] != 0
                if abs(maxVal) < abs(M[yp, y])
                    maxVal = M[yp, y]
                    maxIdx = yp
                end
            end
        end
        
        if y != maxIdx
#            println("swapping row $y with $maxIdx")
            for x = y:y+2*l+1
                if x > n
                    break
                end
                tmp = M[y, x]
                M[y, x] = M[maxIdx, x]
                M[maxIdx, x] = tmp
            end
            tmp = V[maxIdx]
            V[maxIdx] = V[y]
            V[y] = tmp
        end
        
    end

#    println(full(M))
    
    return gaussNoPivot(M, V, l, n)
end

function smToVectorB(matrix::ASCIIString)
    ##
    # Generate vector b based on sparse matrix loaded from file and vector of ones
    ##

    M, n, l = loadMatrix(matrix)
    b = Dict{Int, Float64}()
    for y = 1:n
        b[y] = 0
        yy = div((y - 1), l) * l # matrix row starts from this position
        for x = yy:yy+l
            if x < 1 || x > n
                continue
            end
            b[y] += M[y, x] # * 1
        end
        if l +  y > 0 && l + y <= n
            b[y] += M[l + y] # * 1, element of matrix C
        end
    end

    return M, b, n, l
end

function getError(matrix::ASCIIString)
    A, b, n, l = smToVectorB(matrix)
    
    x1 = gaussNoPivot(A, b, l, n)
    x2 = gaussPivoted(A, b, l, n)

    err1 = round(mean(map(x -> abs(1 - x), x1)) * 100, 2)
    err2 = round(mean(map(x -> abs(1 - x), x2)) * 100, 2)
    
    return err1, err2
end

function writeSolutionToFile(matrix::ASCIIString, vector::ASCIIString, out::ASCIIString)
    ##
    # Produce file with solution
    ##

    A, n, l = loadMatrix(matrix)
    b, n = loadVector(vector)

    x1 = gaussNoPivot(A, b, l, n)
    x2 = gaussPivoted(A, b, l, n)

    open("nopivot.$out", "w") do f
        for i = 1:n
            xi = x1[i]
            write(f, "$xi\n")
        end
    end

    open("pivoted.$out", "w") do f
        for i = 1:n
            xi = x2[i]
            write(f, "$xi\n")
        end
    end
    
end

function writeSolutionToFile(matrix::ASCIIString, out::ASCIIString)
    ##
    # Produce solution for matrix and generated b vector, then write it to file
    ##

    A, b, n, l = smToVectorB(matrix)
    
    x1 = gaussNoPivot(A, b, l, n)
    x2 = gaussPivoted(A, b, l, n)

    open("nopivot.$out", "w") do f
        err = round(mean(map(x -> abs(1 - x), x1)) * 100, 2)
        write(f, "$err%\n")
        for i = 1:n
            xi = x1[i]
            write(f, "$xi\n")
        end
    end

    open("pivoted.$out", "w") do f
        err = round(mean(map(x -> abs(1 - x), x2)) * 100, 2)
        write(f, "$err%\n")
        for i = 1:n
            xi = x2[i]
            write(f, "$xi\n")
        end
    end
end

function test(nrange, lrange, ckrange, n)
    errorNoPivot = Array{Float64}(n)
    errorPivoted = Array{Float64}(n)
    
    for i = 1:n
        l = rand(lrange)
        nx = rand(nrange)
        if nx % l != 0
            nx = nx + (l - nx % l)
        end
        ck = rand(ckrange)
        matrixgen.blockmat(nx, l, ck, "A.txt")
        e1, e2 = getError("A.txt")
        errorNoPivot[i] = e1
        errorPivoted[i] = e2
    end

    return errorNoPivot, errorPivoted
end

end # module blocksys
